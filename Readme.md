# CLEAR C-Robot

This repository contains the Arduino code for the CLEAR C-Robot, along with other documentation for it.
The robot is able to pick samples from a magazine, and place them at a variable position along the beam path.
For this, it uses two stepper motor stages, and a grabbing claw documented in [Documentation/CAD/Grabber/](Documentation/CAD/Grabber/README.md).
Furthermore, a GigE camera with a servo-controlled filter is located so that it sees the object held by the claw from the top, so that a YAG screen "sample" can be used to check beam profile and position.
Finally, up to two thermometers can be connected and read out remotely.

![A view of the whole robot](Documentation/Whole_view_small.png)

The electronics for the robot is located in an adjecent box, which is connected to the robot with long enough cables that it can be placed under the table the robot is sitting on, away from the beam.

![Electronics box](Documentation/Electronic_box_top_small.JPG)

The Arduino code is found in the [4DrobotServer](4DrobotServer/Readme.md) subfolder.
It can be edited with the standard Arduino IDE.
A MATLAB GUI for controlling the robot is incuded, please see the [MatLab](MatLab/README.md) subfolder.

The Arduino- and MATLAB code for this project is licensed under [GNU General Public License v3](LICENSE.txt).
This allows you to use the code freely, with the restriction that if you supply someone with a copy of the code, you must do so under the same terms as you recieved it (e.g. access to potentially modified source code etc.).

The robot was made by A. Gilardi (initial concept, funding to buy first parts), K. Sjobak (Arduino code, MATLAB connection library, comissioning), P. Korysko (Sample magazine and holders, Electronics box, MATLAB GUI, hardware assembly, comissioning) and W. Farabolini (Hardware assembly, comissioning), and Alan Chauchet (Electronics box).

More images and description is found at [https://pkorysko.web.cern.ch/C-Robot.html](https://pkorysko.web.cern.ch/C-Robot.html).