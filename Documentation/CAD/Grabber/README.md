# Gripper

The gripper claw used for the C-Robot is a 3D printed claw that is used to lift and place the sample holders, and to hold it in place during irradiation.
It is driven by a single servomotor, which is controlled by the Arduino.

![](Grabber_view_small.png)

It is derived from a project hosted at [Cults3D](https://cults3d.com/en/3d-model/gadget/gripper-for-6dof-robot-arm) by [Robolab19](https://cults3d.com/en/users/robolab19/creations).
On the project page, it is described as being license as [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/), meaning that it is free to use as long as the original author is attributed, and that access to the work is not restricted by legal terms or technological limits.

The original author has worked on the claw in a git repository at [GitHub](https://github.com/RoboLabHub/RobotArm_v1.0), a copy of this repository at version ef02b545051547ed48d7939e2ff3d6d256c5ad98 is provided in the subfolder [RobotArm_v1.0](RobotArm_v1.0/) (downloaded 25/5/2022).
Here, the individual parts of the grabber itself can be found in the subfolder [RobotArm_v1.0/STL/Gripper](RobotArm_v1.0/STL/Gripper/).
This repository is described as being licensed with a [MIT license](RobotArm_v1.0/LICENSE), which is even less restrictive than CC-BY 3.0.

A copy of the merged "overview" STL files provided by Antonio Gilardi are found in the subfolder [Antonio](Antonio/).
![A 3D rendering of the grabber, in perspective mode](Antonio/GRIPPER_OVERVIEW.png)

Post-print modifications to the claw include adding half-cylinders on the gripping tips which interface with divots on the sample holders, so that the grip reproducibility is increased.

The .STL files can be opened using [FreeCAD](https://www.freecadweb.org/).