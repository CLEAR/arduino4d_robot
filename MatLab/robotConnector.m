% Kyrre Sjobak, Sept. 2021
% For newest version and example for how to use, please see
%   https://gitlab.cern.ch/CLEAR/arduino4d_robot/
%   Subfolder 'MatLab'


classdef robotConnector < handle
    %robotConnector Class for connecting to the CLEAR 4D robot
    %   Please read the documentation-comment in the
    %   robotConnector() constructor function.
    %
    %   The status of the robot is indicated with the variables
    %   in the properties block below, e.g. stepperPos, stepperAbs, etc.
    %
    %   testClass.m contains a few very simple examples on how to use this class.

    % Note: All strings are of "new" type, not 'old'.
    %       This seems to be required by MATLAB TCP/IP library.
    
    properties (SetAccess = protected)
        %Interface status
        
        isReady = 0       %True when this interface is able to recieve commands
                          % Note: The robot may be busy and not responding
                          % immediately, as it is queuing internally.
                    
        lastCommand = ""; %The last command that the robot recieved
                          % (on any interface) while we were connected
        

        %Callback function to GUI etc.
        % Note: This print-only function would normally be replaced by the user.
        % The first argument is a pointer to the object that has been updated.
        % The four last arguments indicate which part has been updated.
        callbackFunction = @(obj, stepper,grabber,temp,filter) ...
            disp(['UPDATE: stepper=',num2str(stepper), ...
                        ', grabber=',num2str(grabber), ...
                        ', temp=',   num2str(temp),    ...
                        ', filter=', num2str(filter)]);

        %Optional extra callback function which is notified whenever a line
        % of text is recieved from the robot.
        textCallbackFunction = @(obj,textLine) disp(textLine);
        
        %% Variables representing the current status of the robot
        
        %Stepper status [X/Y/Z]
        stepperXYZ        = ["X","Y","Z"]; %Names of the axis in this class
        stepperPos        = [NaN,NaN,NaN]; %Current position
        stepperAbs        = [NaN,NaN,NaN]; %Absolute mode?
        stepperLimLo      = [NaN,NaN,NaN]; %Limit switches active?
        stepperLimHi      = [NaN,NaN,NaN]; %Limit switches active?
        stepperInterlock  = NaN;           %Safety interlock enabled?

        stepperIsMoving   = 0;             %Which axis (1/2/3) is moving? 0 if not moving.
        stepperGotoTarget = NaN;           %Where is it going to? NaN if not moving, 0 if ZEROSEEK,
                                           % otherwise it the position it is currently moving to,
                                           %  along the axis indicated by stepperIsMoving.

        %Grabber status
        grabberPos        = NaN;           % Current position of the grabber [deg]

        grabberClosedPos  = NaN;           % Position of the grabber when closed
        grabberOpenPos    = NaN;           % Position of the grabber when open
        grabberMinPos     = NaN;           % Smallest allowed position setting for grabber servo
        grabberMaxPos     = NaN;           % Largest allowed position setting for grabber servo

        grabberIsMoving   = false;         % Is the grabber currently in motion?
        grabberGotoTarget = NaN;           % Where is the grabber going to?
        
        %Filter status
        filterNumFilters  = 2;             % How many filters are connected?
        filterPos         = [NaN,NaN];     % Current positon of the filter [deg]

        filterInPos       = [NaN,NaN];     % Position of the filter when in
        filterOutPos      = [NaN,NaN];     % Position of the filter when out
        filterMinPos      = [NaN,NaN];     % Smallest allowed position setting for filter servo
        filterMaxPos      = [NaN,NaN];     % Largest allowed position setting for filter servo

        filterIsMoving    = [false, false];% Is the filter currently in motion?
        filterGotoTarget  = [NaN,   NaN];  % Where is the filter trying to get to?

        %Temperatures [degC]
        temps = [];

        %True if emergency stop is active, false if not.
        isEmergencyStopped = NaN;

    end
    
    properties (SetAccess = immutable)
        useSerial %Serial (true) or TCP/IP connection (false)?
        address   %Hostname or IP address OR serial port of Arduino, as a string
        TCPport   %TCP port of the arduino, as an int
    end
    
    properties (Access = private)
        TCPconn    %TCP connection object
                   % Connection is automatically destroyed when
                   % robotConnector object is destroyed
        SerialConn % Serial connection object
        conn       % Alias of either TCPconn or SerialConn

        %Special variables for switching between serial (blocking) mode
        % and callback mode
        waitingForReady = false;  %Used by waitForReady()

        %If this is true, callback from TCP to parser is disabled, so
        %parser must be manually triggered. Furthermore the parser will not
        %call obj.callbackFunction.
        callbackEnabled = false;

        %Used by parser:
        linesWithLastCommand = 0; %The number of output (<) lines we have gotten
                                  % since lastCommand
                                  % (the command echo is line #0)
                                  
        commandIsRunning = false; %Gotten all the expected output from a command?
    end
    
    %% USER METHODS

    methods
        function obj = robotConnector(address, callbackFunction, textCallbackFunction, TCPport)
            %robotConnector: Construct and return an instance of this class,
            %                and open a connection to the robot.
            %
            %   Mandatory arguments:
            %   - address :             The hostname or IP address or serial port to connect to, as a string.
            %                             E.g. '192.168.0.42', 'localhost', '4drobot.domain.com', '/dev/ttyACM0', ...
            %
            %   Optional arguments:
            %
            %   - callbackFunction:     Handle to a function that is called whenever
            %                             there is an update of the object status.
            %                             Function signature: @(obj,stepper,grabber,temp,filter)
            %                             The called function can then read the
            %                             status from obj.stepperPos etc.
            %                             Note that if using the _blocking
            %                             functions, this callback is NOT called
            %                             since the program calling the _blocking function
            %                             knows to expect updated state.
            %                             For this reason, it is probably unwise to
            %                             mix blocking and async functions in the same user code.
            %
            %   - textCallbackFunction: Handle to a function that is called whenever
            %                             a line of text is recieved from the robot.
            %                             Function signature: @(obj,textLine)
            %                             The called function gets the last recieved line of text
            %                             as the argument `textLine`;
            %                             by default it is just displayed with `disp()`.
            %
            %   - port:                 The TCP port on address to connect to.
            %                             Defaults to 23 if not provided or set to NaN.
            %                             This is useful for debugging, e.g. redirecting the
            %                             class to a local listener such as `netcat -l 4223`.
            %
            
            %Setup callback
            if exist('callbackFunction','var')
                if ~isa(callbackFunction,'function_handle')
                    error('Argument callbackFunction must be a function handle taking 5 arguments, obj and 4 bools')
                end
                obj.callbackFunction = callbackFunction;
            end

            %Setup textline callback
            if exist('textCallbackFunction','var')
                if ~isa(textCallbackFunction,'function_handle')
                    error('Argument textCallbackFunction must be a function handle taking 2 arguments, obj and textline.')
                end
                obj.textCallbackFunction = textCallbackFunction;
            end

            %Setup connection
            obj.address = address;
            if exist('TCPport','var')
                obj.TCPport = TCPport;
            else
                obj.TCPport = 23;
            end
            
            if startsWith(obj.address, '/')
                disp(strcat("Connecting to serial port ", string(obj.address), "..."))
                obj.SerialConn = serialport(obj.address, 9600);
                obj.useSerial = true;
                obj.conn = obj.SerialConn;
            else
                disp(strcat("Connecting to ", string(obj.address), " on TCP port ", num2str(obj.TCPport), "..."));
                obj.TCPconn = tcpclient(obj.address,obj.TCPport, "Timeout", 2);
                obj.useSerial = false;
                obj.conn = obj.TCPconn;
            end
            disp('Connected!')

            obj.conn.configureTerminator('LF')

            %Wait untill a '$' has been seen before
            %starting to issue commands...
            obj.waitForReady();
            
            %Ready to go!
            obj.enableCallbacks();
            
            %Request population of data
            obj.updateStepperStatus();
            obj.updateGrabberStatus();
            for filterID = 1:obj.filterNumFilters
                obj.updateFilterStatus(filterID);
            end
            obj.updateTemp();
        end
        
        function delete(obj)
            %DELETE Closes the TCP connection
            if ~obj.useSerial
                disp(strcat("Disconnecting from ", string(obj.address), " on port ", num2str(obj.TCPport), "..."));
                clear obj.TCPconn;
            end
        end
        
        %% Commands that can be queued up

        % Trigger download of data
        % Note: The robot may take some time to respond,
        % so please wait for a callback before reading the data!

        function [ok] = updateStepperStatus(obj)
            if ~obj.isReady
                disp('Robot interface not ready')
                ok = false;
                return;
            end
            
            obj.connWrite(['STEPPER STATUS',newline]);
            
            ok=true;
            return
        end

        function [ok] = updateGrabberStatus(obj)
            if ~obj.isReady
                disp('Robot interface not ready')
                ok = false;
                return;
            end
            
            obj.connWrite(['GRABBER STATUS',newline]);
            
            ok=true;
            return
        end

        function [ok] = updateFilterStatus(obj,filterID)
            if ~obj.isReady
                disp('Robot interface not ready')
                ok = false;
                return
            end

            if ~exist('filterID','var')
                error('Input filterID must be set')
            end
            if ~(isnumeric(filterID) && length(filterID) == 1)
                error('Input filterID must be a single integer number')
            end
            if ~(filterID >= 1 && filterID <= obj.filterNumFilters)
                error(['filterID must be between 1 and ', num2str(obj.filterNumFilters)])
            end
            
            obj.connWrite(['FILTER ', num2str(filterID), ' STATUS',newline])

            ok=true;
            return
        end

        function [ok] = updateTemp(obj)
            if ~obj.isReady
                disp('Robot interface not ready')
                ok = false;
                return;
            end
            
            obj.connWrite(['TEMP',newline]);
            
            ok=true;
            return
        end

        % Send an active command
        % Data is returned by callback

        % Grabber controls

        function [ok] = grabberGo(obj,pos)
            if ~obj.isReady
                disp('Robot interface not ready')
                ok = false;
                return;
            end
            
            if ~exist('pos','var')
                error('Input POS must be set')
            end
            if ~(isnumeric(pos) && length(pos) == 1)
                error('Input POS must be a single integer number')
            end

            if ~(pos >= obj.grabberMinPos && pos <= obj.grabberMaxPos)
                error('Requested grabber pos out of range')
            end
            obj.connWrite(['GRABBER POS ', num2str(round(pos)), newline]);

            ok=true;
            return
        end

        function [ok] = grabberOpen(obj)
            if ~obj.isReady
                disp('Robot interface not ready')
                ok = false;
                return;
            end

            obj.connWrite(['GRABBER OPEN',newline]);

            ok=true;
            return
        end

        function [ok] = grabberClose(obj)
            if ~obj.isReady
                disp('Robot interface not ready')
                ok = false;
                return;
            end

            obj.connWrite(['GRABBER CLOSE',newline]);

            ok=true;
            return
        end
        
        % Filter controls

        function [ok] = filterGo(obj,pos,filterID)
            if ~obj.isReady
                disp('Robot interface not ready')
                ok = false;
                return;
            end
            
            if ~exist('pos','var')
                error('Input POS must be set')
            end
            if ~(isnumeric(pos) && length(pos) == 1)
                error('Input POS must be a single integer number')
            end

            if ~exist('filterID','var')
                error('Input filterID must be set')
            end
            if ~(isnumeric(filterID) && length(filterID) == 1)
                error('Input filterID must be a single integer number')
            end
            if ~(filterID >= 1 && filterID <= obj.filterNumFilters)
                error(['filterID must be between 1 and ', num2str(obj.filterNumFilters)])
            end

            if ~(pos >= obj.filterMinPos(filterID) && pos <= obj.filterMaxPos(filterID))
                error('Requested filter pos out of range')
            end

            obj.connWrite(['FILTER ', num2str(filterID), ' POS ', num2str(round(pos)), newline]);

            ok=true;
            return
        end

        function [ok] = filterIn(obj,filterID)
            if ~obj.isReady
                disp('Robot interface not ready')
                ok = false;
                return;
            end

            if ~exist('filterID','var')
                error('Input filterID must be set')
            end
            if ~(isnumeric(filterID) && length(filterID) == 1)
                error('Input filterID must be a single integer number')
            end
            if ~(filterID >= 1 && filterID <= obj.filterNumFilters)
                error(['filterID must be between 1 and ', num2str(obj.filterNumFilters)])
            end

            obj.connWrite(['FILTER ', num2str(filterID), ' IN',newline]);

            ok=true;
            return
        end

        function [ok] = filterOut(obj, filterID)
            if ~obj.isReady
                disp('Robot interface not ready')
                ok = false;
                return;
            end

            if ~exist('filterID','var')
                error('Input filterID must be set')
            end
            if ~(isnumeric(filterID) && length(filterID) == 1)
                error('Input filterID must be a single integer number')
            end
            if ~(filterID >= 1 && filterID <= obj.filterNumFilters)
                error(['filterID must be between 1 and ', num2str(obj.filterNumFilters)])
            end

            obj.connWrite(['FILTER ', num2str(filterID), ' OUT',newline]);

            ok=true;
            return
        end

        % Stepper controls

        function [ok] = stepperZeroseek(obj, xyz)
            if ~obj.isReady
                disp('Robot interface not ready')
                ok = false;
                return;
            end

            if isempty(find(obj.stepperXYZ == xyz, 1))
                error(strcat("xyz must be in ", join(obj.stepperXYZ), " got ",xyz))
            end

            obj.connWrite(strcat("STEPPER ZEROSEEK ", string(xyz), string(newline)));

            ok=true;
            return
        end

        function [ok] = stepperGo(obj, xyz, pos)
            if ~obj.isReady
                disp('Robot interface not ready')
                ok = false;
                return;
            end
            
            if isempty(find(obj.stepperXYZ == xyz, 1))
                error(strcat("xyz must be in ", join(obj.stepperXYZ), " got ",xyz))
            end
            
            if ~exist('pos','var')
                error('Input POS must be set')
            end
            if ~(isnumeric(pos) && length(pos) == 1)
                error('Input POS must be a single integer number')
            end

            obj.connWrite(strcat("STEPPER POS ", string(xyz), " ", string(num2str(round(pos))), string(newline)));

            ok=true;
            return
        end

        % Send a config change command
        % Data is returned by callback

        function [ok] = setStepperInterlock(obj,makeSafe)
            if ~obj.isReady
                disp('Robot interface not ready')
                ok = false;
                return;
            end

            obj.connWrite('STEPPER INTERLOCK ');
            if makeSafe
                obj.connWrite(['ENABLE', newline]);
            else
                obj.connWrite(['DISABLE', newline]);
            end

            ok=true;
            return
        end

        function [ok] = setStepperReset(obj,xyz)
            if ~obj.isReady
                disp('Robot interface not ready')
                ok = false;
                return;
            end

            if isempty(find(obj.stepperXYZ == xyz, 1))
                error(strcat("xyz must be in ", join(obj.stepperXYZ), " got ",xyz))
            end

            obj.connWrite(strcat("STEPPER RESET ", string(xyz), string(newline)));

            %Note:
            % It may be better if this MATLAB command had come come with
            % an automatic stepper status update command,
            % which is queued on the Arduino right after the stepper reset command.
            % Currently the parser will try to reflect the status of the
            % Arduino without actually reading it.

            %ok=obj.updateStepperStatus();
            ok=true;
            return
        end

        % Send a custom command
        function [ok] = sendCustomCommand(obj,cmd)
            if ~obj.isReady
                disp('Robot interface not ready')
                ok = false;
                return;
            end
            obj.connWrite(strcat(string(cmd), string(newline)));
        end
        
        %% Blocking variants of commands
        %  Note: These do not play well with multiple connected users
        %  sending commands, race conditions are VERY LIKELY
        %  However being concurrent to users that are only reading the
        %  status should be OK.
        %
        %  These functions are useful for scripting movement, e.g. go
        %  there, grab, then go somewhere else, but VERY BAD for GUIs even
        %  if they may seem easier. It is intentional that MATLAB freezes
        %  when they are called, and Control-C behaviour is UNDEFINED.
        %
        %  Note that even if the callback which normaly notifies
        %  that the state of the robotConnector object has changed is disabled,
        %  the state is still modified as normal when data is recieved from
        %  the robot. It is up to the user to read e.g. stepperPos after
        %  calling these methods.
        %  Because of this, it's usually not a good idea to mix usage of the
        %  blocking and the nonblocking interface in the same user code.

        function updateStepperStatus_blocking(obj)
            obj.disableCallbacks();
            obj.updateStepperStatus();
            obj.waitForReady();
            obj.enableCallbacks();
        end
        function updateGrabberStatus_blocking(obj)
            obj.disableCallbacks();
            obj.updateGrabberStatus();
            obj.waitForReady();
            obj.enableCallbacks();
        end
        function updateFilterStatus_blocking(obj, filterID)
            obj.disableCallbacks();
            obj.updateFilterStatus(filterID);
            obj.waitForReady();
            obj.enableCallbacks();
        end
        function updateTemp_blocking(obj)
            obj.disableCallbacks();
            obj.updateTemp();
            obj.waitForReady();
            obj.enableCallbacks();
        end
        
        function grabberGo_blocking(obj,pos)
            obj.disableCallbacks();
            obj.grabberGo(pos);
            obj.waitForReady();
            obj.enableCallbacks();
        end
        function grabberOpen_blocking(obj)
            obj.disableCallbacks();
            obj.grabberOpen();
            obj.waitForReady();
            obj.enableCallbacks();
        end
        function grabberClose_blocking(obj)
            obj.disableCallbacks();
            obj.grabberClose();
            obj.waitForReady();
            obj.enableCallbacks();
        end

        function filterGo_blocking(obj,pos)
            obj.disableCallbacks();
            obj.filterGo(pos);
            obj.waitForReady();
            obj.enableCallbacks();
        end
        function filterIn_blocking(obj)
            obj.disableCallbacks();
            obj.filterIn();
            obj.waitForReady();
            obj.enableCallbacks();
        end
        function filterOut_blocking(obj)
            obj.disableCallbacks();
            obj.filterOut();
            obj.waitForReady();
            obj.enableCallbacks();
        end

        function stepperZeroseek_blocking(obj,xyz)
            obj.disableCallbacks();
            obj.stepperZeroseek(xyz);
            obj.waitForReady();
            obj.enableCallbacks();
        end
        function stepperGo_blocking(obj,xyz,pos)
            obj.disableCallbacks();
            obj.stepperGo(xyz,pos);
            obj.waitForReady();
            obj.enableCallbacks();
        end
        
        function setStepperInterlock_blocking(obj,makeSafe)
            obj.disableCallbacks();
            obj.setStepperInterlock(makeSafe);
            obj.waitForReady();
            obj.enableCallbacks();
        end
        function setStepperReset_blocking(obj,xyz)
            obj.disableCallbacks();
            obj.setStepperReset(xyz);
            obj.waitForReady();
            obj.enableCallbacks();
        end
        
        function sendCustomCommand_blocking(obj,cmd)
            obj.disableCallbacks();
            obj.sendCustomCommand(cmd);
            obj.waitForReady();
            obj.enableCallbacks();
        end
        
    end
    
    %% INTERNAL METHODS
    
    methods (Access = protected)
        
        function connWrite(obj, data)
            if obj.useSerial
                write(obj.SerialConn,data,'char')
            else
                obj.TCPconn.write(data)
            end
        end


        function enableCallbacks(obj)
            obj.conn.configureCallback("byte",1, @(srcConn, info) dataCallback(obj))
            obj.callbackEnabled = true;
        end
        
        function disableCallbacks(obj)
            obj.conn.configureCallback("off")
            obj.callbackEnabled = false;
        end
        
        function [ok] = waitForReady(obj)
            %This will return after a new '$' has been recieved.
            % To be used for serialized scripts that ignore the callback.
            %Note: Calling this without any data being incoming will freeze
            % the script.
            
            wasCallbacksOn = obj.callbackEnabled;
            if wasCallbacksOn
                obj.disableCallbacks()
            end
            
            if obj.waitingForReady
                disp('Only one waitForReady() can be active at a given time!')
                ok = false;
                return;
            end
            obj.waitingForReady = true;
            
            %Lazy busywait for data
            % (Note: dataCallback() can handle incomplete lines)
            while (obj.waitingForReady)
                pause(0.1)
                n = obj.conn.BytesAvailable; %for matlab 2022+
                %n = obj.conn.NumBytesAvailable; %for matlab 2021
                if n > 0
                    dataCallback(obj);
                end
            end
            
            if wasCallbacksOn
                obj.enableCallbacks();
            end

            ok=true;
            return
        end
        
        function dataCallback(obj)
            %dataCallback Recieves data from the TCP or serial connection
            %   and updates the class status

            % disp('CB')
            
            n = obj.conn.BytesAvailable; %for matlab 2022+
            %n = obj.conn.NumBytesAvailable; %for matlab 2021
            if n == 0
                %disp('Empty data')
                return
            end
            
            line = obj.conn.readline();
            if isempty(line.char)
                %No newline in data or empty string returned
                disp('Data not ready')
                return
            end

            %Callback function when recieving text
            % by default it's just `disp(line)`
            obj.textCallbackFunction(obj,line)

            %Parse the communication coming from the Arduino!

            if startsWith(line,'$') % READY indication from Arduino
                obj.isReady = true;
                obj.commandIsRunning = false;
                obj.waitingForReady = false;
                %disp('READY')

            elseif startsWith(line,'>') %INPUT-ECHO indication from Arduino

                obj.lastCommand = strtrim(extractAfter(line, '> '));
                obj.linesWithLastCommand = 0;

                %Set commandIsRunning to true if we are expecting output to
                % be parsed on following lines...
                if obj.lastCommand == "STEPPER STATUS" || ...
                   obj.lastCommand == "GRABBER STATUS" || ...
                   obj.lastCommand == "TEMP" || ...
                   obj.lastCommand == "GRABBER OPEN" || ...
                   obj.lastCommand == "GRABBER CLOSE" || ...
                   startsWith(obj.lastCommand,'GRABBER POS ') || ...
                   startsWith(obj.lastCommand, 'STEPPER INTERLOCK ') || ...
                   startsWith(obj.lastCommand, 'STEPPER RESET ') || ...
                   startsWith(obj.lastCommand, 'STEPPER ZEROSEEK ') || ...
                   startsWith(obj.lastCommand, 'STEPPER POS ') || ...
                   ( startsWith(obj.lastCommand, 'FILTER ') && ...
                     ( endsWith(obj.lastCommand, ' STATUS') || ...
                       endsWith(obj.lastCommand, ' IN') || ...
                       endsWith(obj.lastCommand, ' OUT') || ...
                       contains(obj.lastCommand, ' POS ') ))

                    if obj.commandIsRunning
                        obj.commandIsRunning = false; %Reset!
                        error('Implementation error: Command already running?')
                    end

                    %Command was recognized!
                    %Now the next lines should be output from this
                    %command... Change the state of the class!
                    obj.commandIsRunning = true;
                else
                    warn(['Unknown command "', obj.lastCommand,'"'])
                    obj.commandIsRunning = false;
                end

            elseif startsWith(line, '< ') %OUTPUT indication from Arduino, follows INPUT-ECHO
                obj.linesWithLastCommand = obj.linesWithLastCommand + 1;
                
                if obj.commandIsRunning
                    % Parse expected output lines from a known command

                    if obj.lastCommand == "STEPPER STATUS"
                        lineSplit = split(line);

                        if obj.linesWithLastCommand == 1
                            if contains(line,'ENABLED')
                                obj.stepperInterlock = true;
                            elseif contains(line,'DISABLED')
                                obj.stepperInterlock = false;
                            else
                                disp('ERROR: Interlock status could not be determined')
                            end

                        elseif obj.linesWithLastCommand == 2
                            if lineSplit(5) == 'TRUE' %#ok<BDSCA> 
                                obj.isEmergencyStopped = true;
                            elseif lineSplit(5) == 'FALSE' %#ok<BDSCA> 
                                obj.isEmergencyStopped = false;
                            else
                                error(strcat("Unknown bool '", string(lineSplit(5)), "', expected TRUE or FALSE"))
                            end

                        elseif obj.linesWithLastCommand == 4 || obj.linesWithLastCommand == 5 || obj.linesWithLastCommand == 6
                            xyz = lineSplit(3);
                            xyzID = find(obj.stepperXYZ==xyz);
                            if isempty(xyzID)
                                error(strcat("Axis name ", string(xyz), " not recognized"))
                            end
                            
                            obj.stepperPos(xyzID) = str2double(lineSplit(4));
                            obj.stepperAbs(xyzID) = logical(str2double(lineSplit(5)));
                            obj.stepperLimLo(xyzID) = logical(str2double(lineSplit(6)));
                            obj.stepperLimHi(xyzID) = logical(str2double(lineSplit(7)));
                            
                            if obj.linesWithLastCommand == 6
                                %We are done!
                                obj.commandIsRunning = false;
                                if obj.callbackEnabled
                                    obj.callbackFunction(obj,true,false,false,false);
                                end
                            end
                        end

                    elseif obj.lastCommand == "GRABBER STATUS"
                        lineSplit = split(line);
                        if obj.linesWithLastCommand == 1
                            obj.grabberClosedPos = str2double(lineSplit(6));
                            obj.grabberOpenPos   = str2double(lineSplit(9));
                            obj.grabberMinPos = str2double(lineSplit(13));
                            obj.grabberMaxPos   = str2double(lineSplit(16));
                        elseif obj.linesWithLastCommand == 2
                            obj.grabberPos       = str2double(lineSplit(5));
                        elseif obj.linesWithLastCommand == 3
                            if lineSplit(5) == 'TRUE' %#ok<BDSCA> 
                                obj.isEmergencyStopped = true;
                            elseif lineSplit(5) == 'FALSE' %#ok<BDSCA> 
                                obj.isEmergencyStopped = false;
                            else
                                error(strcat("Unknown bool '", string(lineSplit(5)), "', expected TRUE or FALSE"))
                            end

                            obj.commandIsRunning = false;
                            if obj.callbackEnabled
                                obj.callbackFunction(obj,false,true,false,false);
                            end
                        end

                    elseif obj.lastCommand == "TEMP"
                        lineSplit = split(line);
                        if obj.linesWithLastCommand == 1
                            numTemps = str2double(lineSplit(4));
                            obj.temps = ones(numTemps,1)*NaN;
                        else
                            tempIdx = obj.linesWithLastCommand-1;
                            obj.temps(tempIdx) = str2double(lineSplit(4));
                            if obj.temps(tempIdx) == -127
                                obj.temps(tempIdx) = NaN;
                            end
                        end
                        
                        if obj.linesWithLastCommand > length(obj.temps)
                            %Data for sensor N at line N+1,
                            % so stop when N+1 is reached
                            obj.commandIsRunning = false;
                            if obj.callbackEnabled
                                obj.callbackFunction(obj,false,false,true,false);
                            end
                        end
                        
                    elseif obj.lastCommand == "GRABBER OPEN"  || ...
                           obj.lastCommand == "GRABBER CLOSE" || ...
                           startsWith(obj.lastCommand,'GRABBER POS ')
                        lineSplit = split(line);
                        if obj.linesWithLastCommand == 1
                            obj.grabberGotoTarget = str2double(lineSplit(4));
                            obj.isEmergencyStopped = false;
                            %Notify that we have started moving
                            if obj.callbackEnabled
                                obj.callbackFunction(obj,false,true,false,false);
                            end
                        elseif obj.linesWithLastCommand == 2
                            obj.grabberIsMoving = true;
                        elseif startsWith(line, '< GRABBER ERROR EMERGENCY STOP')
                            obj.isEmergencyStopped = true;
                            if obj.callbackEnabled
                                obj.callbackFunction(obj,true,true,false,true);
                            end
                        elseif startsWith(line, '< GRABBER GO FINISHED POS =')
                            obj.grabberGotoTarget = NaN;
                            obj.grabberIsMoving = false;
                            obj.commandIsRunning = false;
                            obj.grabberPos = str2double(lineSplit(7));
                            if obj.callbackEnabled
                                obj.callbackFunction(obj,false,true,false,false);
                            end
                        end
                        
                    elseif startsWith(obj.lastCommand, "FILTER ") && endsWith(obj.lastCommand, " STATUS")
                        lineSplit = split(line);
                        if lineSplit(2) == 'ERROR' %#ok<BDSCA> 
                            obj.commandIsRunning = false;
                            error('ERROR from robot parsing FILTER <id> STATUS command')
                        end

                        if obj.linesWithLastCommand == 1
                            filterID = uint16(str2num(lineSplit(3))); %#ok<ST2NM> 
                            if ~(filterID >= 1 && filterID <= obj.filterNumFilters)
                                error(['Internal error: filterID must be between 1 and ', num2str(obj.filterNumFilters)])
                            end

                            obj.filterInPos(filterID)  = str2double(lineSplit(7));
                            obj.filterOutPos(filterID) = str2double(lineSplit(10));
                            obj.filterMinPos(filterID) = str2double(lineSplit(14));
                            obj.filterMaxPos(filterID) = str2double(lineSplit(17));
                        elseif obj.linesWithLastCommand == 2
                            filterID = uint16(str2num(lineSplit(3))); %#ok<ST2NM> 
                            if ~(filterID >= 1 && filterID <= obj.filterNumFilters)
                                error(['Internal error: filterID must be between 1 and ', num2str(obj.filterNumFilters)])
                            end

                            obj.filterPos(filterID)    = str2double(lineSplit(6));
                        elseif obj.linesWithLastCommand == 3
                            if lineSplit(5) == 'TRUE' %#ok<BDSCA> 
                                obj.isEmergencyStopped = true;
                            elseif lineSplit(5) == 'FALSE' %#ok<BDSCA> 
                                obj.isEmergencyStopped = false;
                            else
                                error(strcat("Unknown bool '", string(lineSplit(5)), "', expected TRUE or FALSE"))
                            end

                            obj.commandIsRunning = false;
                            if obj.callbackEnabled
                                obj.callbackFunction(obj,false,false,false,true);
                            end
                        end

                    elseif startsWith(obj.lastCommand, "FILTER ") && ...
                            (endsWith(obj.lastCommand, " IN") || ...
                             endsWith(obj.lastCommand, " OUT") || ...
                             contains(obj.lastCommand, " POS ") )
                        lineSplit = split(line);
                        filterID = uint16(str2num(lineSplit(3))); %#ok<ST2NM>

                        if ~(filterID >= 1 && filterID <= obj.filterNumFilters)
                            error(['Internal error: filterID must be between 1 and ', num2str(obj.filterNumFilters)])
                        end

                        if obj.linesWithLastCommand == 1
                            obj.filterGotoTarget(filterID) = str2double(lineSplit(5));
                            obj.isEmergencyStopped = false;
                            %Notify that we have started moving
                            if obj.callbackEnabled
                                obj.callbackFunction(obj,false,false,false,true);
                            end
                        elseif startsWith(line, '< FILTER ') && endsWith(line, ' MOVING...')
                            obj.filterIsMoving(filterID) = true;
                        elseif startsWith(line, '< FILTER ') && endsWith(line, ' ERROR EMERGENCY STOP')
                            obj.isEmergencyStopped = true;
                            if obj.callbackEnabled
                                obj.callbackFunction(obj,true,true,false,true);
                            end
                        elseif startsWith(line, '< FILTER ') && contains(line, ' GO FINISHED POS = ')
                            obj.filterGotoTarget(filterID) = NaN;
                            obj.filterIsMoving(filterID) = false;
                            obj.commandIsRunning = false;
                            obj.filterPos(filterID) = str2double(lineSplit(8));
                            if obj.callbackEnabled
                                obj.callbackFunction(obj,false,false,false,true);
                            end
                        end

                    elseif startsWith(obj.lastCommand, 'STEPPER INTERLOCK ')
                        lineSplit = split(line);
                        if lineSplit(5) == "ENABLED"
                            obj.stepperInterlock = true;
                        elseif lineSplit(5) == "DISABLED"
                            obj.stepperInterlock = false;
                        else
                            error('Did not understand reply for STEPPER INTERLOCK command?')
                        end
                        obj.commandIsRunning = false;
                        if obj.callbackEnabled
                            obj.callbackFunction(obj,true,false,false,false)
                        end

                    elseif startsWith(obj.lastCommand, 'STEPPER RESET ')
                        lineSplit = split(line);
                        xyz = lineSplit(5);
                        xyzID = find(obj.stepperXYZ==xyz);
                        if isempty(xyzID)
                            error(strcat("Axis name ", string(xyz), " not recognized"))
                        end

                        obj.stepperPos(xyzID) = 0;
                        obj.stepperAbs(xyzID) = false;

                        obj.commandIsRunning = false;
                        if obj.callbackEnabled
                            obj.callbackFunction(obj,true,false,false,false);
                        end

                    elseif startsWith(obj.lastCommand, 'STEPPER ZEROSEEK ') || ...
                           startsWith(obj.lastCommand, 'STEPPER POS ')

                        lineSplitCMD = split(obj.lastCommand);
                        xyz = lineSplitCMD(3);

                        xyzID = find(obj.stepperXYZ==xyz);
                        if isempty(xyzID)
                            error(strcat("Axis name ", string(xyz), " not recognized"))
                        end

                        if startsWith(obj.lastCommand, 'STEPPER ZEROSEEK ')
                            obj.stepperGotoTarget = 0;
                            obj.stepperAbs(xyzID) = false;
                        end

                        lineSplit = split(line);

                        if startsWith(line, '< STEPPER GO FINISHED') % This is always the final output line
                            obj.stepperIsMoving = 0;
                            obj.stepperGotoTarget = NaN;
                            obj.stepperPos(xyzID) = str2double(lineSplit(8));

                            if startsWith(obj.lastCommand, 'STEPPER ZEROSEEK ')
                                if obj.stepperLimLo(xyzID) == false && obj.stepperLimHi(xyzID) == false && obj.isEmergencyStopped == false

                                    %sucess!
                                    obj.stepperLimLo(xyzID) = true;
                                    obj.stepperAbs(xyzID) = true;
                                else
                                    %Failed zeroseek.
                                end
                            end

                            obj.commandIsRunning = false;
                            if obj.callbackEnabled
                                obj.callbackFunction(obj,true,false,false,false);
                            end

                        elseif line.startsWith("< STEPPER GOTO ")
                            obj.stepperGotoTarget = str2double(lineSplit(5));

                        elseif line.startsWith("< STEPPER MOVING ")
                            obj.stepperIsMoving = xyzID;
                            obj.isEmergencyStopped = false; %We assume for now...
                            if obj.callbackEnabled
                                obj.callbackFunction(obj,true,false,false,false);
                            end
                            %Some of these will be set to true if
                            % (1) we touch a limit switch, or
                            % (2) we do a successful zeroseek
                            obj.stepperLimLo(xyzID) = false;
                            obj.stepperLimHi(xyzID) = false;

                        elseif line.startsWith("< STEPPER ERROR LIMIT SWITCH")
                            obj.stepperLimLo(xyzID) = logical(str2double(lineSplit(8)));
                            obj.stepperLimHi(xyzID) = logical(str2double(lineSplit(11)));

                        elseif line.startsWith("< STEPPER ERROR MAXSTEPS_MOVE")
                            % Treat it like an emergency stop, it effectively is...
                            obj.isEmergencyStopped = true;

                        elseif line.startsWith("< STEPPER ERROR IMPOSSIBLE LIMIT SWITCH STATUS")
                            % Treat it like an emergency stop, it effectively is...
                            obj.isEmergencyStopped = true;

                        elseif line.startsWith("< STEPPER ERROR EMERGENCY STOP")
                            obj.isEmergencyStopped = true;
                            if obj.callbackEnabled
                                obj.callbackFunction(obj,true,true,false,true);
                            end

                        elseif line == "< STEPPER ERROR XY INTERLOCK"
                            assert(obj.stepperInterlock, "Status reflection error, did not expect interlock to be enabled")

                        elseif line == "< STEPPER WARNING XY INTERLOCK IGNORED"
                            assert(obj.stepperInterlock, "Status reflection error, expected interlock to be enabled")

                        end

                    else
                        error(['lastCommand="',obj.lastCommand,'" unknown to parser even if commandIsRunning==true?'])
                    end

                else
                    %Ignore output lines from unknown command
                    % (commandIsRunning is false)
                end

            elseif startsWith(line, '@') % LOCAL-ERROR / DEBUG / INFO from Arduino
                % Ignore it

            else
                error('Unknown return type indication (first character in line) in data from robot!')

            end

        end
    end
end

