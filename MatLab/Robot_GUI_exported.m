classdef Robot_GUI_exported < matlab.apps.AppBase

    % Properties that correspond to app components
    properties (Access = public)
        UIFigure                      matlab.ui.Figure
        GridLayout                    matlab.ui.container.GridLayout
        % LEFT PANEL
        LeftPanel                     matlab.ui.container.Panel
        %Zero seeks pane
        ZeroseeksLabel                matlab.ui.control.Label
        AbsoluteXTextArea             matlab.ui.control.EditField
        ZeroseekXButton               matlab.ui.control.Button
        AbsLamp_X                     matlab.ui.control.Lamp
        AbsoluteYTextArea             matlab.ui.control.EditField
        ZeroseekYButton               matlab.ui.control.Button
        AbsLamp_Y                     matlab.ui.control.Lamp
        AbsoluteZTextArea             matlab.ui.control.EditField
        ZeroseekZButton               matlab.ui.control.Button
        AbsLamp_Z                     matlab.ui.control.Lamp   
        %Limit switches pane
        LimitswitchesLabel            matlab.ui.control.Label
        MinLabel                      matlab.ui.control.Label
        MaxLabel                      matlab.ui.control.Label
        XLabel                        matlab.ui.control.Label
        AbsLamp_X_min                 matlab.ui.control.Lamp
        AbsLamp_X_max                 matlab.ui.control.Lamp
        YLabel                        matlab.ui.control.Label
        AbsLamp_Y_min                 matlab.ui.control.Lamp
        AbsLamp_Y_max                 matlab.ui.control.Lamp
        ZLabel                        matlab.ui.control.Label
        AbsLamp_Z_min                 matlab.ui.control.Lamp
        AbsLamp_Z_max                 matlab.ui.control.Lamp
        %Emergency button pane
        Emergency_Button              matlab.ui.control.Lamp
        EmergencybuttonLabel          matlab.ui.control.Label
        %Stepper status pane
        StepperStatusTextArea         matlab.ui.control.EditField
        StepperstatusLabel            matlab.ui.control.Label
        %Grabber status pane
        GrabberstatusLabel            matlab.ui.control.Label
        GrabberstatusTextArea         matlab.ui.control.TextArea
        %Update status button
        UpdatestatusButton            matlab.ui.control.Button
        %Temperatures pane
        TemperaturesTextAreaLabel     matlab.ui.control.Label
        Tempprobe1CLabel              matlab.ui.control.Label
        TemperaturesTextArea          matlab.ui.control.TextArea
        Tempprobe2CLabel              matlab.ui.control.Label
        TemperaturesTextArea_2        matlab.ui.control.TextArea
        GettemperaturesButton         matlab.ui.control.Button
        
        BlackLine                     matlab.ui.control.Label
        BlacklineLabel_9              matlab.ui.control.Label
        BlacklineLabel_8              matlab.ui.control.Label
        BlacklineLabel_7              matlab.ui.control.Label
        BlacklineLabel_6              matlab.ui.control.Label
        BlacklineLabel_13             matlab.ui.control.Label
        BlacklineLabel_10             matlab.ui.control.Label
        BlacklineLabel_5              matlab.ui.control.Label
        BlacklineLabel_4              matlab.ui.control.Label
        BlacklineLabel                matlab.ui.control.Label
        
        
        %CENTER PANEL
        CenterPanel                   matlab.ui.container.Panel
        TabGroup                      matlab.ui.container.TabGroup
        PlotsTab                      matlab.ui.container.Tab
        PositionplotsLabel            matlab.ui.control.Label
        Image2                        matlab.ui.control.Image
        BlacklineLabel_11             matlab.ui.control.Label 
        UIAxes_6                      matlab.ui.control.UIAxes
        UIAxes_5                      matlab.ui.control.UIAxes
        UIAxes_4                      matlab.ui.control.UIAxes
        DebugTab                      matlab.ui.container.Tab
        BlacklineLabel_12             matlab.ui.control.Label
        DebugLabel                    matlab.ui.control.Label
        RawHistory                    matlab.ui.control.ListBox
        
        %RIGHT PANEL
        RightPanel                    matlab.ui.container.Panel
        ControlsLabel                 matlab.ui.control.Label
        SelecttheholdertopickupLabel  matlab.ui.control.Label
        %Holder buttons
        Panel                         matlab.ui.container.Panel
        GridLayout2                   matlab.ui.container.GridLayout
        %GrabHolderButtons             matlab.ui.container.ButtonGroup
        GrabHolder_32                 matlab.ui.control.StateButton
        GrabHolder_31                 matlab.ui.control.StateButton
        GrabHolder_30                 matlab.ui.control.StateButton
        GrabHolder_29                 matlab.ui.control.StateButton
        GrabHolder_28                 matlab.ui.control.StateButton
        GrabHolder_27                 matlab.ui.control.StateButton
        GrabHolder_26                 matlab.ui.control.StateButton
        GrabHolder_25                 matlab.ui.control.StateButton
        GrabHolder_23                 matlab.ui.control.StateButton
        GrabHolder_12                 matlab.ui.control.StateButton
        GrabHolder_24                 matlab.ui.control.StateButton
        GrabHolder_22                 matlab.ui.control.StateButton
        GrabHolder_21                 matlab.ui.control.StateButton
        GrabHolder_20                 matlab.ui.control.StateButton
        GrabHolder_19                 matlab.ui.control.StateButton
        GrabHolder_18                 matlab.ui.control.StateButton
        GrabHolder_17                 matlab.ui.control.StateButton
        GrabHolder_16                 matlab.ui.control.StateButton
        GrabHolder_15                 matlab.ui.control.StateButton
        GrabHolder_14                 matlab.ui.control.StateButton
        GrabHolder_13                 matlab.ui.control.StateButton
        GrabHolder_11                 matlab.ui.control.StateButton
        GrabHolder_10                 matlab.ui.control.StateButton
        GrabHolder_09                 matlab.ui.control.StateButton
        GrabHolder_08                 matlab.ui.control.StateButton
        GrabHolder_07                 matlab.ui.control.StateButton
        GrabHolder_06                 matlab.ui.control.StateButton
        GrabHolder_05                 matlab.ui.control.StateButton
        GrabHolder_04                 matlab.ui.control.StateButton
        GrabHolder_03                 matlab.ui.control.StateButton
        GrabHolder_02                 matlab.ui.control.StateButton
        GrabHolder_01                 matlab.ui.control.StateButton
        %Robot control
        PutHolderInBeam               matlab.ui.control.Button
        BringbackholderButton         matlab.ui.control.Button
        %Filter pane
        BlackLine1                    matlab.ui.control.Label
        BlackLine2                    matlab.ui.control.Label
        BlackLine3                     matlab.ui.control.Label
        FilterLabel1                  matlab.ui.control.Label
        Filter1INButton               matlab.ui.control.Button
        Filter1OUTButton              matlab.ui.control.Button
        FilterLabel2                  matlab.ui.control.Label
        Filter2INButton               matlab.ui.control.Button
        Filter2OUTButton              matlab.ui.control.Button
        %Last command
        LastCommandTextArea           matlab.ui.control.TextArea
        LastCommandTextAreaLabel      matlab.ui.control.Label
        %C-robot image   
        Image                         matlab.ui.control.Image
        XpositioninbeammmEditField    matlab.ui.control.NumericEditField
        XpositioninbeammmEditFieldLabel  matlab.ui.control.Label
        BlacklineLabel_3              matlab.ui.control.Label        
        StatusandChecksLabel          matlab.ui.control.Label    
    end

    % Properties that correspond to apps with auto-reflow
    properties (Access = private)
        onePanelWidth = 576;
        twoPanelWidth = 768;
    end


    properties (Access = private)
        conn = [];


        Pos_hold_idx = NaN;
        Pos_hold_X = NaN;
        Pos_hold_Y = NaN;
        Pos_hold_Z = NaN;

        StepperIsMoving = NaN;

        Pos_holders = [225 200 2200; ...
                       225 1825 2200; ...
                       717 200 2200; ...
                       717 1825 2200; ...
                       1198 200 2200; ...
                       1198 1825 2200; ...
                       1679 200 2200; ...
                       1679 1825 2200; ...
                       2160 200 2200; ...
                       2160 1825 2200; ...
                       2641 200 2200; ...
                       2641 1825 2200; ...
                       3122 200 2200; ...
                       3122 1825 2200; ...
                       3603 200 2200; ...
                       3603 1825 2200; ...
                       4084 200 2200; ...
                       4084 1825 2200; ...
                       4565 200 2200; ...
                       4565 1825 2200; ...
                       5046 200 2200; ...
                       5046 1825 2200; ...
                       5527 200 2200; ...
                       5527 1825 2200; ...
                       6008 200 2200; ...
                       6008 1825 2200; ...
                       6489 200 2200; ...
                       6489 1825 2200; ...
                       6970 200 2200; ...
                       6970 1825 2200; ...
                       7451 200 2200; ...
                       7451 1825 2200; ...
                       7932 200 2200; ...
                       8413 1825 2200];

        %List of all holder buttons,
        % not possible to generate programattically in AppDesigner
        % so it has to be manually maintained (to be sure it's in the right order)...
        grabHolderButts = [];
        filterINButts = [];
        filterOUTButts = [];
        

    end

    methods (Access = private)

        function robotCallBack(app,stepper,grabber,temp, filters)
            disp(['UPDATE: stepper=',num2str(stepper), ...
                        ', grabber=',num2str(grabber), ...
                        ', temp=',   num2str(temp), ...
                        ', filter=', num2str(filters)]);
            disp(app.conn)

            if temp
                app.TemperaturesTextArea.Value = num2str(app.conn.temps(1));
                app.TemperaturesTextArea_2.Value = num2str(app.conn.temps(2));
            end

            if stepper

                %Local copies of state variables for easy referencing
                Pos_X = app.conn.stepperPos(1);
                Pos_Y = app.conn.stepperPos(2);
                Pos_Z = app.conn.stepperPos(3);

                Abs_X = app.conn.stepperAbs(1);
                Abs_Y = app.conn.stepperAbs(2);
                Abs_Z = app.conn.stepperAbs(3);

                LimLo_X = app.conn.stepperLimLo(1);
                LimLo_Y = app.conn.stepperLimLo(2);
                LimLo_Z = app.conn.stepperLimLo(3);

                LimHi_X = app.conn.stepperLimHi(1);
                LimHi_Y = app.conn.stepperLimHi(2);
                LimHi_Z = app.conn.stepperLimHi(3);

                app.StepperIsMoving = app.conn.stepperIsMoving;

                %LastCommand

                app.LastCommandTextArea.Value = num2str(app.conn.lastCommand);

                %Plotting the position

%                 axes(app.UIAxes_6);

                scatter(app.UIAxes_6,Pos_X, Pos_Z,100,"red",'filled');
                rectangle(app.UIAxes_6,'Position', [0 1400 8000 1600],'FaceColor',[1, .8, .8, 0.5],'EdgeColor','r','LineWidth',1)
                text(app.UIAxes_6,2600,1750,'X/Y interlock region')
                text(app.UIAxes_6,2600,2400,'Storage/Beam tank')
                rectangle(app.UIAxes_6,'Position', [0 2000 8000 1000],'FaceColor',[0, 1, 1, 0.5],'EdgeColor','blue','LineWidth',1)


%                 axes(app.UIAxes_5);

                scatter(app.UIAxes_5,Pos_X, Pos_Y,100,"red",'filled');
                rectangle(app.UIAxes_5,'Position', [0 0 8000 2000],'FaceColor',[0, 1, 1, 0.5],'EdgeColor','blue','LineWidth',1)
                text(app.UIAxes_5,3000,1000,'Storage tank')
                rectangle(app.UIAxes_5,'Position', [0 5280 8000 2720],'FaceColor',[0, 1, 1, 0.5],'EdgeColor','blue','LineWidth',1)
                text(app.UIAxes_5,3000,6750,'Beam tank')

%                 axes(app.UIAxes_4);

                scatter(app.UIAxes_4,Pos_Y, Pos_Z,100,"red",'filled');
                rectangle(app.UIAxes_4,'Position', [0 2000 2000 1000],'FaceColor',[0, 1, 1, 0.5],'EdgeColor','blue','LineWidth',1)
                text(app.UIAxes_4,450,2500,'Storage')
                rectangle(app.UIAxes_4,'Position', [5280 2000 2720 1000],'FaceColor',[0, 1, 1, 0.5],'EdgeColor','blue','LineWidth',1)
                text(app.UIAxes_4,6200,2500,'Beam')
                rectangle(app.UIAxes_4,'Position', [0 1400 8000 1600],'FaceColor',[1, .8, .8, 0.5],'EdgeColor','r','LineWidth',1)
                text(app.UIAxes_4,2600,1750,'X/Y interlock region')


%                 axes(app.UIAxes_3);
%
%                 plot3(app.UIAxes_3,app.Pos_X,app.Pos_Y, app.Pos_Z,'.','Color','r','MarkerSize',40)
%
%                 app.UIAxes_3.View = [90 45];
%                 app.UIAxes_3.Projection = 'perspective';

%                  H=[0 8000 0 8000 0 8000 0 8000; 0 0 2000 2000 0 0 2000 2000; 3000 3000 3000 3000 1500 1500 1500 1500]; %Vertices of the cube
%                  S=[1 2 4 3; 1 2 6 5; 1 3 7 5; 3 4 8 7; 2 4 8 6; 5 6 8 7]; %Surfaces of the cube
%                  hold(app.UIAxes_3,"on")
%                  for i=1:size(S,1)
%                      Si=S(i,:);
%                      fill3(app.UIAxes_3,H(1,Si),H(2,Si),H(3,Si),[0, 1, 1],'facealpha',0.6)
%                  end
%
%                  H=[0 8000 0 8000 0 8000 0 8000; 6000 6000 8000 8000 6000 6000 8000 8000; 3000 3000 3000 3000 1500 1500 1500 1500]; %Vertices of the cube
%                  S=[1 2 4 3; 1 2 6 5; 1 3 7 5; 3 4 8 7; 2 4 8 6; 5 6 8 7]; %Surfaces of the cube
%                  hold(app.UIAxes_3,"on")
%                  for i=1:size(S,1)
%                      Si=S(i,:);
%                      fill3(app.UIAxes_3,H(1,Si),H(2,Si),H(3,Si),[0, 1, 1],'facealpha',0.6)
%                  end
%                  hold(app.UIAxes_3,"off")


                %Light and text for Absolute/Relative
                if Abs_X == 1
                    app.AbsoluteXTextArea.Value = "Absolute";
                    app.AbsLamp_X.Color = 'green';
                else
                    app.AbsoluteXTextArea.Value = "Relative";
                    app.AbsLamp_X.Color = 'red';
                end

                if Abs_Y == 1
                    app.AbsoluteYTextArea.Value = "Absolute";
                    app.AbsLamp_Y.Color = 'green';
                else
                    app.AbsoluteYTextArea.Value = "Relative";
                    app.AbsLamp_Y.Color = 'red';
                end

                if Abs_Z == 1
                    app.AbsoluteZTextArea.Value = "Absolute";
                    app.AbsLamp_Z.Color = 'green';
                else
                    app.AbsoluteZTextArea.Value = "Relative";
                    app.AbsLamp_Z.Color = 'red';
                end

                %Stepper is moving Text area
                if app.conn.stepperIsMoving == 1
                   app.StepperStatusTextArea.Value = "X stepper is moving";
                end

                if app.conn.stepperIsMoving == 2
                   app.StepperStatusTextArea.Value = "Y stepper is moving";
                end

                if app.conn.stepperIsMoving == 3
                   app.StepperStatusTextArea.Value = "Z stepper is moving";
                end

                if app.StepperIsMoving == 0
                   app.StepperStatusTextArea.Value = "Stepper is not moving";
                end

                %Light for limit switches
                if LimLo_X == 1
                   app.AbsLamp_X_min.Color = 'green';
                else
                   app.AbsLamp_X_min.Color = [0.8 0.8 0.8];
                end
                if LimLo_Y == 1
                   app.AbsLamp_Y_min.Color = 'green';
                else
                   app.AbsLamp_Y_min.Color = [0.8 0.8 0.8];
                end
                if LimLo_Z == 1
                   app.AbsLamp_Z_min.Color = 'green';
                else
                   app.AbsLamp_Z_min.Color = [0.8 0.8 0.8];
                end

                if LimHi_X == 1
                   app.AbsLamp_X_max.Color = 'green';
                else
                   app.AbsLamp_X_max.Color = [0.8 0.8 0.8];
                end
                if LimHi_Y == 1
                   app.AbsLamp_Y_max.Color = 'green';
                else
                   app.AbsLamp_Y_max.Color = [0.8 0.8 0.8];
                end
                if LimHi_Z == 1
                   app.AbsLamp_Z_max.Color = 'green';
                else
                   app.AbsLamp_Z_max.Color = [0.8 0.8 0.8];
                end

                %Light for Emergency Button
                if app.conn.isEmergencyStopped == 1
                   app.Emergency_Button.Color = 'green';
                else
                   app.Emergency_Button.Color = [0.8 0.8 0.8];
                end
            end

            %Status of the grabber
            if grabber
                if app.conn.grabberPos == app.conn.grabberOpenPos
                    app.GrabberstatusTextArea.Value = "Open";
                elseif app.conn.grabberPos == app.conn.grabberClosedPos
                    app.GrabberstatusTextArea.Value = "Close";
                else
                    app.GrabberstatusTextArea.Value = num2str(app.conn.grabberPos);
                end
            end

            %Status of the filters
            if filters
                if app.conn.filterPos(1) == app.conn.filterInPos(1)
                   app.Filter1INButton.BackgroundColor = 'green';
                   app.Filter1OUTButton.BackgroundColor = [0.96 0.96 0.96];
                elseif app.conn.filterPos(1) == app.conn.filterOutPos(1)
                   app.Filter1INButton.BackgroundColor = [0.96 0.96 0.96];
                   app.Filter1OUTButton.BackgroundColor = 'red';
                end
                if app.conn.filterPos(2) == app.conn.filterInPos(2)
                   app.Filter2INButton.BackgroundColor = 'green';
                   app.Filter2OUTButton.BackgroundColor = [0.96 0.96 0.96];
                elseif app.conn.filterPos(2) == app.conn.filterOutPos(2)
                   app.Filter2INButton.BackgroundColor = [0.96 0.96 0.96];
                   app.Filter2OUTButton.BackgroundColor = 'red';
                end
            end

           

        end

        function textCallBack(app,textLine)
            txt = convertStringsToChars(textLine);

            if isempty(app.RawHistory.Items) || isempty(app.RawHistory.Items{1})
                app.RawHistory.Items{1} = txt;
            else
                app.RawHistory.Items{end+1} = txt;
            end

            scroll(app.RawHistory,'bottom');

        end

         function grabHolder(app,Pos_h_X,Pos_h_Y,Pos_h_Z)

            app.conn.updateGrabberStatus
            %app.conn.grabberOpen;
            app.conn.grabberGo(117);

            app.conn.stepperGo('Z',50);

            app.conn.stepperGo('X',Pos_h_X);
            app.conn.stepperGo('Y',Pos_h_Y);
            app.conn.stepperGo('Z',Pos_h_Z);
            app.conn.grabberClose;

            app.conn.updateGrabberStatus

         end


    end


    % Callbacks that handle component events
    methods (Access = private)

        % Code that executes after component creation
        function startupFcn(app)
            app.conn = robotConnector("172.18.211.7",@(obj,stepper,grabber,temp,filters) app.robotCallBack(stepper,grabber,temp,filters),@(obj,textLine) app.textCallBack(textLine));
            %app.conn = robotConnector("/dev/ttyACM0",@(obj,stepper,grabber,temp,filters) app.robotCallBack(stepper,grabber,temp,filters),@(obj,textLine) app.textCallBack(textLine));


            %List of all holder buttons,
            % not possible to generate programattically in AppDesigner
            % so it has to be manually maintained (to be sure it's in the right order)...
            % Also, it cannot be made earlier because the GUI must first create itself...
            app.grabHolderButts = [app.GrabHolder_01, app.GrabHolder_02, app.GrabHolder_03, ...
                                   app.GrabHolder_04, app.GrabHolder_05, app.GrabHolder_06, ...
                                   app.GrabHolder_07, app.GrabHolder_08, app.GrabHolder_09, ...
                                   app.GrabHolder_10, app.GrabHolder_11, app.GrabHolder_12, ...
                                   app.GrabHolder_13, app.GrabHolder_14, app.GrabHolder_15, ...
                                   app.GrabHolder_16, app.GrabHolder_17, app.GrabHolder_18, ...
                                   app.GrabHolder_19, app.GrabHolder_20, app.GrabHolder_21, ...
                                   app.GrabHolder_22, app.GrabHolder_23, app.GrabHolder_24, ...
                                   app.GrabHolder_25, app.GrabHolder_26, app.GrabHolder_27, ...
                                   app.GrabHolder_28, app.GrabHolder_29, app.GrabHolder_30, ...
                                   app.GrabHolder_31, app.GrabHolder_32];
            
            app.filterINButts=[app.Filter1INButton, app.Filter2INButton];
            app.filterOUTButts=[app.Filter1OUTButton, app.Filter2OUTButton];

        end

        % Changes arrangement of the app based on UIFigure width
        function updateAppLayout(app, event)
            currentFigureWidth = app.UIFigure.Position(3);
            if(currentFigureWidth <= app.onePanelWidth)
                % Change to a 3x1 grid
                app.GridLayout.RowHeight = {816, 816, 816};
                app.GridLayout.ColumnWidth = {'1x'};
                app.CenterPanel.Layout.Row = 1;
                app.CenterPanel.Layout.Column = 1;
                app.LeftPanel.Layout.Row = 2;
                app.LeftPanel.Layout.Column = 1;
                app.RightPanel.Layout.Row = 3;
                app.RightPanel.Layout.Column = 1;
            elseif (currentFigureWidth > app.onePanelWidth && currentFigureWidth <= app.twoPanelWidth)
                % Change to a 2x2 grid
                app.GridLayout.RowHeight = {816, 816};
                app.GridLayout.ColumnWidth = {'1x', '1x'};
                app.CenterPanel.Layout.Row = 1;
                app.CenterPanel.Layout.Column = [1,2];
                app.LeftPanel.Layout.Row = 2;
                app.LeftPanel.Layout.Column = 1;
                app.RightPanel.Layout.Row = 2;
                app.RightPanel.Layout.Column = 2;
            else
                % Change to a 1x3 grid
                app.GridLayout.RowHeight = {'1x'};
                app.GridLayout.ColumnWidth = {220, '1x', 224};
                app.LeftPanel.Layout.Row = 1;
                app.LeftPanel.Layout.Column = 1;
                app.CenterPanel.Layout.Row = 1;
                app.CenterPanel.Layout.Column = 2;
                app.RightPanel.Layout.Row = 1;
                app.RightPanel.Layout.Column = 3;
            end
        end

        % Button pushed function: ZeroseekXButton
        function ZeroseekXButtonPushed(app, event)
            app.conn.stepperZeroseek('X');
        end

        % Button pushed function: ZeroseekYButton
        function ZeroseekYButtonPushed(app, event)
            app.conn.stepperZeroseek('Y');
        end

        % Button pushed function: ZeroseekZButton
        function ZeroseekZButtonPushed(app, event)
            app.conn.stepperZeroseek('Z');
        end

        % Callback function: GettemperaturesButton, TemperaturesTextArea,
        % TemperaturesTextArea_2
        function GettempsButtonPushed(app, event)
            app.conn.updateTemp;
        end

        % Button pushed function: PutHolderInBeam
        function PutHolderInBeamPushed(app, event)
            app.conn.updateGrabberStatus

            app.conn.stepperGo('Z',50);
            app.conn.stepperGo('Y',7265);
            app.conn.stepperGo('X',app.XpositioninbeammmEditField.Value*7803/460+95*7803/460);
            app.conn.stepperGo('Z',2220);

            %Disable itself, enable bringback button
            app.BringbackholderButton.Enable = true;

            %Disable off all storage position buttons when in beam
            for i = 1:length(app.grabHolderButts)
                app.grabHolderButts(i).Enable = false;
            end
        end

        % Button pushed function: BringbackholderButton
        function BringbackholderButtonPushed(app, event)
            if isnan(app.Pos_hold_idx)
                error('No known holder position to bringback to?')
            end

            app.conn.stepperGo('Z',50);

            app.conn.stepperGo('Y',app.Pos_hold_Y);
            app.conn.stepperGo('X',app.Pos_hold_X);

            app.conn.stepperGo('Z',app.Pos_hold_Z);
            %app.conn.grabberOpen;
            app.conn.grabberGo(117);

            app.conn.updateGrabberStatus()

            app.Pos_hold_idx = NaN;
            app.Pos_hold_X   = NaN;
            app.Pos_hold_Y   = NaN;
            app.Pos_hold_Z   = NaN;

            %Turn off all storage position buttons
            for i = 1:length(app.grabHolderButts)
                app.grabHolderButts(i).Value = false;
                app.grabHolderButts(i).Enable = true;
            end
            %Disable itself
            app.BringbackholderButton.Enable=false;

        end

        % Button pushed function: UpdatestatusButton
        function UpdatestatusButtonPushed(app, event)
            app.conn.updateStepperStatus;
            app.conn.updateGrabberStatus
        end

        % Value changed function: GrabHolder_01, GrabHolder_02,
        % GrabHolder_03, GrabHolder_04, GrabHolder_05, GrabHolder_06,
        % GrabHolder_07, GrabHolder_08, GrabHolder_09, GrabHolder_10,
        % GrabHolder_11, GrabHolder_12, GrabHolder_13, GrabHolder_14,
        % GrabHolder_15, GrabHolder_16, GrabHolder_17, GrabHolder_18,
        % GrabHolder_19, GrabHolder_20, GrabHolder_21, GrabHolder_22,
        % GrabHolder_23, GrabHolder_24, GrabHolder_25, GrabHolder_26,
        % GrabHolder_27, GrabHolder_28, GrabHolder_29, GrabHolder_30,
        % GrabHolder_31, GrabHolder_32
        function GrabHolder_ValueChanged(app, event)
            %This function is called for ALL the GrabHolder_* buttons.
            % It basically emulates a Toggle button group,
            % but lets us set the position of the buttons freely.
            % Note that only a physical user click on the button generates
            %  this callback, not just a .Value change (even if MATLAB says so)

            %Which one was clicked?
            idx = find(event.Source==app.grabHolderButts);
            if isempty(idx)
                error('Internal error: Could not find button in array')
            end

            value = event.Source.Value;
            %Ignore button presses (incl. programmatical ones) to "off".
            if value ~= 1
                if idx == app.Pos_hold_idx
                    %Disp cannot turn off the active holder!
                    app.grabHolderButts(idx).Value = 1;
                end
                return;
            end

            %Turn off all other buttons
            for i = 1:length(app.grabHolderButts)
                if i == idx
                    continue
                end
                app.grabHolderButts(i).Value = 0;
            end

            %Update GUI class status
            app.Pos_hold_idx = idx;
            app.Pos_hold_X =  app.Pos_holders(idx,1);
            app.Pos_hold_Y =  app.Pos_holders(idx,2);
            app.Pos_hold_Z =  app.Pos_holders(idx,3);

            %Actually move the robot
            app.conn.updateGrabberStatus();
            %app.conn.grabberOpen();
            app.conn.grabberGo(117);

            app.conn.stepperGo('Z',50);
            app.conn.stepperGo('X',app.Pos_hold_X);
            app.conn.stepperGo('Y',app.Pos_hold_Y);
            app.conn.stepperGo('Z',app.Pos_hold_Z);

            app.conn.grabberClose();
            app.conn.updateGrabberStatus();
        end
    
        % Button pushed function: FilterINButton
        function FilterINButtonPushed(app, event)
            idx = find(event.Source==app.filterINButts);
            if isempty(idx)
                error('Internal error: Could not find button in array')
            end
            app.conn.filterIn(idx);
        end

        % Button pushed function: FilterOUTButton
        function FilterOUTButtonPushed(app, event)
            idx = find(event.Source==app.filterOUTButts);
            if isempty(idx)
                error('Internal error: Could not find button in array')
            end
            app.conn.filterOut(idx);
        end

%         % Button pushed function: FilterINButton
%         function Filter2INButtonPushed(app, event)
%             app.conn.filterIn(2);
%         end
% 
%         % Button pushed function: FilterOUTButton
%         function Filter2OUTButtonPushed(app, event)
%             app.conn.filterOut(2);
%         end

    end

     % Component initialization
    methods (Access = private)

        % Create UIFigure and components
        function createComponents(app)

            % Create UIFigure and hide until all components are created
            app.UIFigure = uifigure('Visible', 'off');
            app.UIFigure.AutoResizeChildren = 'off';
            app.UIFigure.Color = [1 1 1];
            app.UIFigure.Position = [100 100 883 816];
            app.UIFigure.Name = 'MATLAB App';
            app.UIFigure.SizeChangedFcn = createCallbackFcn(app, @updateAppLayout, true);

            % Create GridLayout
            app.GridLayout = uigridlayout(app.UIFigure);
            app.GridLayout.ColumnWidth = {220, '1x', 224};
            app.GridLayout.RowHeight = {'1x'};
            app.GridLayout.ColumnSpacing = 0;
            app.GridLayout.RowSpacing = 0;
            app.GridLayout.Padding = [0 0 0 0];
            app.GridLayout.Scrollable = 'on';

            % Create LeftPanel
            app.LeftPanel = uipanel(app.GridLayout);
            app.LeftPanel.Layout.Row = 1;
            app.LeftPanel.Layout.Column = 1;
            
            % Create StatusandChecksLabel
            app.StatusandChecksLabel = uilabel(app.UIFigure);
            app.StatusandChecksLabel.HorizontalAlignment = 'center';
            app.StatusandChecksLabel.FontSize = 20;
            app.StatusandChecksLabel.FontWeight = 'bold';
            app.StatusandChecksLabel.Position = [20 767 180 43];
            app.StatusandChecksLabel.Text = 'Status and Checks';
            
            % Create BlacklineLabel
            app.BlacklineLabel = uilabel(app.LeftPanel);
            app.BlacklineLabel.BackgroundColor = [0 0 0];
            app.BlacklineLabel.FontSize = 2;
            app.BlacklineLabel.Position = [1 769 218 2];
            app.BlacklineLabel.Text = 'Black line';

            % Create ZeroseeksLabel
            app.ZeroseeksLabel = uilabel(app.LeftPanel);
            app.ZeroseeksLabel.HorizontalAlignment = 'center';
            app.ZeroseeksLabel.FontSize = 14;
            app.ZeroseeksLabel.FontWeight = 'bold';
            app.ZeroseeksLabel.Position = [58 727 103 41];
            app.ZeroseeksLabel.Text = 'Zero seeks';

            % Create ZeroseekXButton
            app.ZeroseekXButton = uibutton(app.LeftPanel, 'push');
            app.ZeroseekXButton.ButtonPushedFcn = createCallbackFcn(app, @ZeroseekXButtonPushed, true);
            app.ZeroseekXButton.Position = [12 689 98 46];
            app.ZeroseekXButton.Text = 'Zero seek X';

            % Create ZeroseekYButton
            app.ZeroseekYButton = uibutton(app.LeftPanel, 'push');
            app.ZeroseekYButton.ButtonPushedFcn = createCallbackFcn(app, @ZeroseekYButtonPushed, true);
            app.ZeroseekYButton.Position = [12 637 98 46];
            app.ZeroseekYButton.Text = 'Zero seek Y';

            % Create ZeroseekZButton
            app.ZeroseekZButton = uibutton(app.LeftPanel, 'push');
            app.ZeroseekZButton.ButtonPushedFcn = createCallbackFcn(app, @ZeroseekZButtonPushed, true);
            app.ZeroseekZButton.Position = [12 585 98 46];
            app.ZeroseekZButton.Text = 'Zero seek Z';

            % Create AbsLamp_X
            app.AbsLamp_X = uilamp(app.LeftPanel);
            app.AbsLamp_X.Position = [192 701 20 20];

            % Create AbsLamp_Z
            app.AbsLamp_Z = uilamp(app.LeftPanel);
            app.AbsLamp_Z.Position = [192 598 20 20];

            % Create AbsLamp_Y
            app.AbsLamp_Y = uilamp(app.LeftPanel);
            app.AbsLamp_Y.Position = [192 649 20 20];

            % Create GrabberstatusTextArea
            app.GrabberstatusTextArea = uitextarea(app.LeftPanel);
            app.GrabberstatusTextArea.HorizontalAlignment = 'center';
            app.GrabberstatusTextArea.Position = [47 218 128 24];

            % Create AbsoluteXTextArea
            app.AbsoluteXTextArea = uieditfield(app.LeftPanel, 'text');
            app.AbsoluteXTextArea.HorizontalAlignment = 'center';
            app.AbsoluteXTextArea.Position = [118 700 65 22];

            % Create AbsoluteYTextArea
            app.AbsoluteYTextArea = uieditfield(app.LeftPanel, 'text');
            app.AbsoluteYTextArea.HorizontalAlignment = 'center';
            app.AbsoluteYTextArea.Position = [118 648 65 22];

            % Create AbsoluteZTextArea
            app.AbsoluteZTextArea = uieditfield(app.LeftPanel, 'text');
            app.AbsoluteZTextArea.HorizontalAlignment = 'center';
            app.AbsoluteZTextArea.Position = [118 597 65 22];

            % Create StepperstatusLabel
            app.StepperstatusLabel = uilabel(app.LeftPanel);
            app.StepperstatusLabel.HorizontalAlignment = 'center';
            app.StepperstatusLabel.FontSize = 14;
            app.StepperstatusLabel.FontWeight = 'bold';
            app.StepperstatusLabel.Position = [58 312 103 41];
            app.StepperstatusLabel.Text = 'Stepper status';

            % Create StepperStatusTextArea
            app.StepperStatusTextArea = uieditfield(app.LeftPanel, 'text');
            app.StepperStatusTextArea.HorizontalAlignment = 'center';
            app.StepperStatusTextArea.FontSize = 14;
            app.StepperStatusTextArea.FontWeight = 'bold';
            app.StepperStatusTextArea.FontColor = [1 0 0];
            app.StepperStatusTextArea.Position = [26 290 168 24];

            % Create LimitswitchesLabel
            app.LimitswitchesLabel = uilabel(app.LeftPanel);
            app.LimitswitchesLabel.HorizontalAlignment = 'center';
            app.LimitswitchesLabel.FontSize = 14;
            app.LimitswitchesLabel.FontWeight = 'bold';
            app.LimitswitchesLabel.Position = [59 543 102 41];
            app.LimitswitchesLabel.Text = 'Limit switches';

            % Create AbsLamp_X_min
            app.AbsLamp_X_min = uilamp(app.LeftPanel);
            app.AbsLamp_X_min.Position = [73 497 16 16];
            app.AbsLamp_X_min.Color = [0.8 0.8 0.8];

            % Create MinLabel
            app.MinLabel = uilabel(app.LeftPanel);
            app.MinLabel.HorizontalAlignment = 'center';
            app.MinLabel.Position = [64 513 34 41];
            app.MinLabel.Text = 'Min';

            % Create MaxLabel
            app.MaxLabel = uilabel(app.LeftPanel);
            app.MaxLabel.HorizontalAlignment = 'center';
            app.MaxLabel.Position = [119 513 34 41];
            app.MaxLabel.Text = 'Max';

            % Create XLabel
            app.XLabel = uilabel(app.LeftPanel);
            app.XLabel.HorizontalAlignment = 'center';
            app.XLabel.Position = [22 485 34 41];
            app.XLabel.Text = 'X';

            % Create YLabel
            app.YLabel = uilabel(app.LeftPanel);
            app.YLabel.HorizontalAlignment = 'center';
            app.YLabel.Position = [22 452 34 41];
            app.YLabel.Text = 'Y';

            % Create ZLabel
            app.ZLabel = uilabel(app.LeftPanel);
            app.ZLabel.HorizontalAlignment = 'center';
            app.ZLabel.Position = [22 418 34 41];
            app.ZLabel.Text = 'Z';

            % Create AbsLamp_Y_min
            app.AbsLamp_Y_min = uilamp(app.LeftPanel);
            app.AbsLamp_Y_min.Position = [73 464 16 16];
            app.AbsLamp_Y_min.Color = [0.8 0.8 0.8];

            % Create AbsLamp_Z_min
            app.AbsLamp_Z_min = uilamp(app.LeftPanel);
            app.AbsLamp_Z_min.Position = [73 431 16 16];
            app.AbsLamp_Z_min.Color = [0.8 0.8 0.8];

            % Create AbsLamp_X_max
            app.AbsLamp_X_max = uilamp(app.LeftPanel);
            app.AbsLamp_X_max.Position = [128 497 16 16];
            app.AbsLamp_X_max.Color = [0.8 0.8 0.8];

            % Create AbsLamp_Y_max
            app.AbsLamp_Y_max = uilamp(app.LeftPanel);
            app.AbsLamp_Y_max.Position = [128 464 16 16];
            app.AbsLamp_Y_max.Color = [0.8 0.8 0.8];

            % Create AbsLamp_Z_max
            app.AbsLamp_Z_max = uilamp(app.LeftPanel);
            app.AbsLamp_Z_max.Position = [128 431 16 16];
            app.AbsLamp_Z_max.Color = [0.8 0.8 0.8];

            % Create GrabberstatusLabel
            app.GrabberstatusLabel = uilabel(app.LeftPanel);
            app.GrabberstatusLabel.HorizontalAlignment = 'center';
            app.GrabberstatusLabel.FontSize = 14;
            app.GrabberstatusLabel.FontWeight = 'bold';
            app.GrabberstatusLabel.Position = [58 238 105 41];
            app.GrabberstatusLabel.Text = 'Grabber status';



            % Create BlacklineLabel_4
            app.BlacklineLabel_4 = uilabel(app.LeftPanel);
            app.BlacklineLabel_4.BackgroundColor = [0 0 0];
            app.BlacklineLabel_4.FontSize = 2;
            app.BlacklineLabel_4.Position = [1 578 218 1];
            app.BlacklineLabel_4.Text = 'Black line';

            % Create GettemperaturesButton
            app.GettemperaturesButton = uibutton(app.LeftPanel, 'push');
            app.GettemperaturesButton.ButtonPushedFcn = createCallbackFcn(app, @GettempsButtonPushed, true);
            app.GettemperaturesButton.Tag = 'button_temps';
            app.GettemperaturesButton.Position = [67 6 86 36];
            app.GettemperaturesButton.Text = {'Get '; 'temperatures'};

            % Create BlacklineLabel_5
            app.BlacklineLabel_5 = uilabel(app.LeftPanel);
            app.BlacklineLabel_5.BackgroundColor = [0 0 0];
            app.BlacklineLabel_5.FontSize = 2;
            app.BlacklineLabel_5.Position = [1 415 218 1];
            app.BlacklineLabel_5.Text = 'Black line';

            % Create Tempprobe1CLabel
            app.Tempprobe1CLabel = uilabel(app.LeftPanel);
            app.Tempprobe1CLabel.Position = [9 87 104 22];
            app.Tempprobe1CLabel.Text = 'Temp probe 1 (°C)';

            % Create Tempprobe2CLabel
            app.Tempprobe2CLabel = uilabel(app.LeftPanel);
            app.Tempprobe2CLabel.Position = [9 49 104 22];
            app.Tempprobe2CLabel.Text = 'Temp probe 2 (°C)';

            % Create BlacklineLabel_6
            app.BlacklineLabel_6 = uilabel(app.LeftPanel);
            app.BlacklineLabel_6.BackgroundColor = [0 0 0];
            app.BlacklineLabel_6.FontSize = 2;
            app.BlacklineLabel_6.Position = [1 276 218 1];
            app.BlacklineLabel_6.Text = 'Black line';

            % Create BlacklineLabel_7
            app.BlacklineLabel_7 = uilabel(app.LeftPanel);
            app.BlacklineLabel_7.BackgroundColor = [0 0 0];
            app.BlacklineLabel_7.FontSize = 2;
            app.BlacklineLabel_7.Position = [1 148 218 1];
            app.BlacklineLabel_7.Text = 'Black line';

            % Create BlacklineLabel_8
            app.BlacklineLabel_8 = uilabel(app.LeftPanel);
            app.BlacklineLabel_8.BackgroundColor = [0.8 0.8 0.8];
            app.BlacklineLabel_8.FontSize = 2;
            app.BlacklineLabel_8.Position = [45 546 129 1];
            app.BlacklineLabel_8.Text = 'Black line';

            % Create BlacklineLabel_9
            app.BlacklineLabel_9 = uilabel(app.LeftPanel);
            app.BlacklineLabel_9.BackgroundColor = [0.8 0.8 0.8];
            app.BlacklineLabel_9.FontSize = 2;
            app.BlacklineLabel_9.Position = [65 520 99 1];
            app.BlacklineLabel_9.Text = 'Black line';

            % Create TemperaturesTextArea_2
            app.TemperaturesTextArea_2 = uitextarea(app.LeftPanel);
            app.TemperaturesTextArea_2.ValueChangedFcn = createCallbackFcn(app, @GettempsButtonPushed, true);
            app.TemperaturesTextArea_2.Editable = 'off';
            app.TemperaturesTextArea_2.HorizontalAlignment = 'center';
            app.TemperaturesTextArea_2.Position = [120 48 93 24];

            % Create TemperaturesTextArea
            app.TemperaturesTextArea = uitextarea(app.LeftPanel);
            app.TemperaturesTextArea.ValueChangedFcn = createCallbackFcn(app, @GettempsButtonPushed, true);
            app.TemperaturesTextArea.Editable = 'off';
            app.TemperaturesTextArea.HorizontalAlignment = 'center';
            app.TemperaturesTextArea.Position = [119 85 93 24];

            % Create TemperaturesTextAreaLabel
            app.TemperaturesTextAreaLabel = uilabel(app.LeftPanel);
            app.TemperaturesTextAreaLabel.HorizontalAlignment = 'right';
            app.TemperaturesTextAreaLabel.FontSize = 14;
            app.TemperaturesTextAreaLabel.FontWeight = 'bold';
            app.TemperaturesTextAreaLabel.Position = [62 123 97 22];
            app.TemperaturesTextAreaLabel.Text = 'Temperatures';

            % Create UpdatestatusButton
            app.UpdatestatusButton = uibutton(app.LeftPanel, 'push');
            app.UpdatestatusButton.ButtonPushedFcn = createCallbackFcn(app, @UpdatestatusButtonPushed, true);
            app.UpdatestatusButton.FontSize = 18;
            app.UpdatestatusButton.Position = [49 164 127 29];
            app.UpdatestatusButton.Text = 'Update status';

            % Create BlacklineLabel_10
            app.BlacklineLabel_10 = uilabel(app.LeftPanel);
            app.BlacklineLabel_10.BackgroundColor = [0 0 0];
            app.BlacklineLabel_10.FontSize = 2;
            app.BlacklineLabel_10.Position = [1 209 218 1];
            app.BlacklineLabel_10.Text = 'Black line';

            % Create BlacklineLabel_13
            app.BlacklineLabel_13 = uilabel(app.LeftPanel);
            app.BlacklineLabel_13.BackgroundColor = [0 0 0];
            app.BlacklineLabel_13.FontSize = 2;
            app.BlacklineLabel_13.Position = [1 353 218 1];
            app.BlacklineLabel_13.Text = 'Black line';

            % Create EmergencybuttonLabel
            app.EmergencybuttonLabel = uilabel(app.LeftPanel);
            app.EmergencybuttonLabel.HorizontalAlignment = 'center';
            app.EmergencybuttonLabel.FontSize = 14;
            app.EmergencybuttonLabel.FontWeight = 'bold';
            app.EmergencybuttonLabel.Position = [45 378 128 41];
            app.EmergencybuttonLabel.Text = 'Emergency button';

            % Create Emergency_Button
            app.Emergency_Button = uilamp(app.LeftPanel);
            app.Emergency_Button.Position = [99 362 20 20];

            % Create CenterPanel
            app.CenterPanel = uipanel(app.GridLayout);
            app.CenterPanel.Layout.Row = 1;
            app.CenterPanel.Layout.Column = 2;

            % Create TabGroup
            app.TabGroup = uitabgroup(app.CenterPanel);
            app.TabGroup.TabLocation = 'bottom';
            app.TabGroup.Position = [7 1 425 808];

            % Create PlotsTab
            app.PlotsTab = uitab(app.TabGroup);
            app.PlotsTab.Title = 'Plots';

            % Create UIAxes_4
            app.UIAxes_4 = uiaxes(app.PlotsTab);
            title(app.UIAxes_4, 'Position Z/Y')
            xlabel(app.UIAxes_4, 'Y (steps)')
            ylabel(app.UIAxes_4, 'Z (steps)')
            zlabel(app.UIAxes_4, 'Z')
            app.UIAxes_4.Toolbar.Visible = 'off';
            app.UIAxes_4.XLim = [0 8000];
            app.UIAxes_4.YLim = [0 3000];
            app.UIAxes_4.YDir = 'reverse';
            app.UIAxes_4.XTick = [0 2000 4000 6000 8000];
            app.UIAxes_4.XTickLabel = {'0'; '2000'; '4000'; '6000'; '8000'};
            app.UIAxes_4.YTick = [0 500 1000 1500 2000 2500 3000];
            app.UIAxes_4.YTickLabel = {'0'; '500'; '1000'; '1500'; '2000'; '2500'; '3000'};
            app.UIAxes_4.XGrid = 'on';
            app.UIAxes_4.YGrid = 'on';
            app.UIAxes_4.FontSize = 12;
            app.UIAxes_4.Position = [13 6 403 236];

            % Create UIAxes_5
            app.UIAxes_5 = uiaxes(app.PlotsTab);
            title(app.UIAxes_5, 'Position Y/X')
            xlabel(app.UIAxes_5, 'X (steps)')
            ylabel(app.UIAxes_5, 'Y (steps)')
            zlabel(app.UIAxes_5, 'Z')
            app.UIAxes_5.Toolbar.Visible = 'off';
            app.UIAxes_5.XLim = [0 8000];
            app.UIAxes_5.YLim = [0 8000];
            app.UIAxes_5.XTick = [0 2000 4000 6000 8000];
            app.UIAxes_5.XTickLabel = {'0'; '2000'; '4000'; '6000'; '8000'};
            app.UIAxes_5.YTick = [0 2000 4000 6000 8000];
            app.UIAxes_5.YTickLabel = {'0'; '2000'; '4000'; '6000'; '8000'};
            app.UIAxes_5.XGrid = 'on';
            app.UIAxes_5.YGrid = 'on';
            app.UIAxes_5.FontSize = 12;
            app.UIAxes_5.Position = [13 496 403 238];

            % Create UIAxes_6
            app.UIAxes_6 = uiaxes(app.PlotsTab);
            title(app.UIAxes_6, 'Position Z/X')
            xlabel(app.UIAxes_6, 'X (steps)')
            ylabel(app.UIAxes_6, 'Z (steps)')
            zlabel(app.UIAxes_6, 'Z')
            app.UIAxes_6.Toolbar.Visible = 'off';
            app.UIAxes_6.XLim = [0 8000];
            app.UIAxes_6.YLim = [0 3000];
            app.UIAxes_6.YDir = 'reverse';
            app.UIAxes_6.XTick = [0 2000 4000 6000 8000];
            app.UIAxes_6.XTickLabel = {'0'; '2000'; '4000'; '6000'; '8000'};
            app.UIAxes_6.YTick = [0 500 1000 1500 2000 2500 3000];
            app.UIAxes_6.YTickLabel = {'0'; '500'; '1000'; '1500'; '2000'; '2500'; '3000'};
            app.UIAxes_6.XGrid = 'on';
            app.UIAxes_6.YGrid = 'on';
            app.UIAxes_6.FontSize = 12;
            app.UIAxes_6.Position = [13 253 403 238];

            % Create PositionplotsLabel
            app.PositionplotsLabel = uilabel(app.PlotsTab);
            app.PositionplotsLabel.HorizontalAlignment = 'center';
            app.PositionplotsLabel.FontSize = 20;
            app.PositionplotsLabel.FontWeight = 'bold';
            app.PositionplotsLabel.Position = [118 745 180 43];
            app.PositionplotsLabel.Text = 'Position plots';

            % Create BlacklineLabel_11
            app.BlacklineLabel_11 = uilabel(app.PlotsTab);
            app.BlacklineLabel_11.BackgroundColor = [0 0 0];
            app.BlacklineLabel_11.FontSize = 2;
            app.BlacklineLabel_11.Position = [-3 744 418 2];
            app.BlacklineLabel_11.Text = 'Black line';

            % Create Image2
            app.Image2 = uiimage(app.PlotsTab);
            app.Image2.Position = [13 649 53 77];
            app.Image2.ImageSource = 'e_beam.png';

            % Create DebugTab
            app.DebugTab = uitab(app.TabGroup);
            app.DebugTab.Title = 'Debug';

            % Create RawHistory
            app.RawHistory = uilistbox(app.DebugTab);
            app.RawHistory.Items = {};
            app.RawHistory.FontSize = 10.4;
            app.RawHistory.Position = [7 9 409 721];
            app.RawHistory.Value = {};

            % Create DebugLabel
            app.DebugLabel = uilabel(app.DebugTab);
            app.DebugLabel.HorizontalAlignment = 'center';
            app.DebugLabel.FontSize = 20;
            app.DebugLabel.FontWeight = 'bold';
            app.DebugLabel.Position = [118 745 180 43];
            app.DebugLabel.Text = 'Debug';

            % Create BlacklineLabel_12
            app.BlacklineLabel_12 = uilabel(app.DebugTab);
            app.BlacklineLabel_12.BackgroundColor = [0 0 0];
            app.BlacklineLabel_12.FontSize = 2;
            app.BlacklineLabel_12.Position = [-5 745 435 2];
            app.BlacklineLabel_12.Text = 'Black line';

            % Create RightPanel
            app.RightPanel = uipanel(app.GridLayout);
            app.RightPanel.Layout.Row = 1;
            app.RightPanel.Layout.Column = 3;

            % Create BringbackholderButton
            app.BringbackholderButton = uibutton(app.RightPanel, 'push');
            app.BringbackholderButton.ButtonPushedFcn = createCallbackFcn(app, @BringbackholderButtonPushed, true);
            app.BringbackholderButton.FontWeight = 'bold';
            app.BringbackholderButton.Enable = 'off';
            app.BringbackholderButton.Position = [51 223 132 38];
            app.BringbackholderButton.Text = 'Bring back holder';

            % Create SelecttheholdertopickupLabel
            app.SelecttheholdertopickupLabel = uilabel(app.RightPanel);
            app.SelecttheholdertopickupLabel.HorizontalAlignment = 'center';
            app.SelecttheholdertopickupLabel.FontSize = 14;
            app.SelecttheholdertopickupLabel.FontWeight = 'bold';
            app.SelecttheholdertopickupLabel.Position = [24 725 188 38];
            app.SelecttheholdertopickupLabel.Text = 'Select the holder to pick up';

            % Create ControlsLabel
            app.ControlsLabel = uilabel(app.RightPanel);
            app.ControlsLabel.HorizontalAlignment = 'center';
            app.ControlsLabel.FontSize = 20;
            app.ControlsLabel.FontWeight = 'bold';
            app.ControlsLabel.Position = [22 767 180 43];
            app.ControlsLabel.Text = 'Controls';

            % Create BlacklineLabel_3
            app.BlacklineLabel_3 = uilabel(app.RightPanel);
            app.BlacklineLabel_3.BackgroundColor = [0 0 0];
            app.BlacklineLabel_3.FontSize = 2;
            app.BlacklineLabel_3.Position = [1 769 218 2];
            app.BlacklineLabel_3.Text = 'Black line';

            % Create PutHolderInBeam
            app.PutHolderInBeam = uibutton(app.RightPanel, 'push');
            app.PutHolderInBeam.ButtonPushedFcn = createCallbackFcn(app, @PutHolderInBeamPushed, true);
            app.PutHolderInBeam.FontWeight = 'bold';
            app.PutHolderInBeam.Position = [55 269 124 38];
            app.PutHolderInBeam.Text = 'Put holder in beam';


            % Create XpositioninbeammmEditFieldLabel
            app.XpositioninbeammmEditFieldLabel = uilabel(app.RightPanel);
            app.XpositioninbeammmEditFieldLabel.HorizontalAlignment = 'center';
            app.XpositioninbeammmEditFieldLabel.Position = [49 318 80 28];
            app.XpositioninbeammmEditFieldLabel.Text = {'X position '; 'in beam (mm)'};

            % Create XpositioninbeammmEditField
            app.XpositioninbeammmEditField = uieditfield(app.RightPanel, 'numeric');
            app.XpositioninbeammmEditField.HorizontalAlignment = 'center';
            app.XpositioninbeammmEditField.Position = [135 321 54 22];
            app.XpositioninbeammmEditField.Value = 200;

            % Create Panel
            app.Panel = uipanel(app.RightPanel);
            app.Panel.Position = [20 363 194 364];

            % Create GridLayout2
            app.GridLayout2 = uigridlayout(app.Panel);
            app.GridLayout2.ColumnWidth = {80, 80};
            app.GridLayout2.RowHeight = {'1x', '1x', '1x', '1x', '1x', '1x', '1x', '1x', '1x', '1x', '1x', '1x', '1x', '1x', '1x', '1x'};
            app.GridLayout2.RowSpacing = 2;
            app.GridLayout2.Padding = [10 0 0 0];
            
        

            % Create GrabHolder_01
            app.GrabHolder_01 = uibutton(app.GridLayout2, 'state');
            app.GrabHolder_01.ValueChangedFcn = createCallbackFcn(app, @GrabHolder_ValueChanged, true);
            app.GrabHolder_01.Text = '1';
            app.GrabHolder_01.Layout.Row = 1;
            app.GrabHolder_01.Layout.Column = 1;

            % Create GrabHolder_02
            app.GrabHolder_02 = uibutton(app.GridLayout2, 'state');
            app.GrabHolder_02.ValueChangedFcn = createCallbackFcn(app, @GrabHolder_ValueChanged, true);
            app.GrabHolder_02.Text = '2';
            app.GrabHolder_02.Layout.Row = 1;
            app.GrabHolder_02.Layout.Column = 2;

            % Create GrabHolder_03
            app.GrabHolder_03 = uibutton(app.GridLayout2, 'state');
            app.GrabHolder_03.ValueChangedFcn = createCallbackFcn(app, @GrabHolder_ValueChanged, true);
            app.GrabHolder_03.Text = '3';
            app.GrabHolder_03.Layout.Row = 2;
            app.GrabHolder_03.Layout.Column = 1;

            % Create GrabHolder_04
            app.GrabHolder_04 = uibutton(app.GridLayout2, 'state');
            app.GrabHolder_04.ValueChangedFcn = createCallbackFcn(app, @GrabHolder_ValueChanged, true);
            app.GrabHolder_04.Text = '4';
            app.GrabHolder_04.Layout.Row = 2;
            app.GrabHolder_04.Layout.Column = 2;

            % Create GrabHolder_05
            app.GrabHolder_05 = uibutton(app.GridLayout2, 'state');
            app.GrabHolder_05.ValueChangedFcn = createCallbackFcn(app, @GrabHolder_ValueChanged, true);
            app.GrabHolder_05.Text = '5';
            app.GrabHolder_05.Layout.Row = 3;
            app.GrabHolder_05.Layout.Column = 1;

            % Create GrabHolder_06
            app.GrabHolder_06 = uibutton(app.GridLayout2, 'state');
            app.GrabHolder_06.ValueChangedFcn = createCallbackFcn(app, @GrabHolder_ValueChanged, true);
            app.GrabHolder_06.Text = '6';
            app.GrabHolder_06.Layout.Row = 3;
            app.GrabHolder_06.Layout.Column = 2;

            % Create GrabHolder_07
            app.GrabHolder_07 = uibutton(app.GridLayout2, 'state');
            app.GrabHolder_07.ValueChangedFcn = createCallbackFcn(app, @GrabHolder_ValueChanged, true);
            app.GrabHolder_07.Text = '7';
            app.GrabHolder_07.Layout.Row = 4;
            app.GrabHolder_07.Layout.Column = 1;

            % Create GrabHolder_08
            app.GrabHolder_08 = uibutton(app.GridLayout2, 'state');
            app.GrabHolder_08.ValueChangedFcn = createCallbackFcn(app, @GrabHolder_ValueChanged, true);
            app.GrabHolder_08.Text = '8';
            app.GrabHolder_08.Layout.Row = 4;
            app.GrabHolder_08.Layout.Column = 2;

            % Create GrabHolder_09
            app.GrabHolder_09 = uibutton(app.GridLayout2, 'state');
            app.GrabHolder_09.ValueChangedFcn = createCallbackFcn(app, @GrabHolder_ValueChanged, true);
            app.GrabHolder_09.Text = '9';
            app.GrabHolder_09.Layout.Row = 5;
            app.GrabHolder_09.Layout.Column = 1;

            % Create GrabHolder_10
            app.GrabHolder_10 = uibutton(app.GridLayout2, 'state');
            app.GrabHolder_10.ValueChangedFcn = createCallbackFcn(app, @GrabHolder_ValueChanged, true);
            app.GrabHolder_10.Text = '10';
            app.GrabHolder_10.Layout.Row = 5;
            app.GrabHolder_10.Layout.Column = 2;

            % Create GrabHolder_11
            app.GrabHolder_11 = uibutton(app.GridLayout2, 'state');
            app.GrabHolder_11.ValueChangedFcn = createCallbackFcn(app, @GrabHolder_ValueChanged, true);
            app.GrabHolder_11.Text = '11';
            app.GrabHolder_11.Layout.Row = 6;
            app.GrabHolder_11.Layout.Column = 1;

            % Create GrabHolder_13
            app.GrabHolder_13 = uibutton(app.GridLayout2, 'state');
            app.GrabHolder_13.ValueChangedFcn = createCallbackFcn(app, @GrabHolder_ValueChanged, true);
            app.GrabHolder_13.Text = '13';
            app.GrabHolder_13.Layout.Row = 7;
            app.GrabHolder_13.Layout.Column = 1;

            % Create GrabHolder_14
            app.GrabHolder_14 = uibutton(app.GridLayout2, 'state');
            app.GrabHolder_14.ValueChangedFcn = createCallbackFcn(app, @GrabHolder_ValueChanged, true);
            app.GrabHolder_14.Text = '14';
            app.GrabHolder_14.Layout.Row = 7;
            app.GrabHolder_14.Layout.Column = 2;

            % Create GrabHolder_15
            app.GrabHolder_15 = uibutton(app.GridLayout2, 'state');
            app.GrabHolder_15.ValueChangedFcn = createCallbackFcn(app, @GrabHolder_ValueChanged, true);
            app.GrabHolder_15.Text = '15';
            app.GrabHolder_15.Layout.Row = 8;
            app.GrabHolder_15.Layout.Column = 1;

            % Create GrabHolder_16
            app.GrabHolder_16 = uibutton(app.GridLayout2, 'state');
            app.GrabHolder_16.ValueChangedFcn = createCallbackFcn(app, @GrabHolder_ValueChanged, true);
            app.GrabHolder_16.Text = '16';
            app.GrabHolder_16.Layout.Row = 8;
            app.GrabHolder_16.Layout.Column = 2;

            % Create GrabHolder_17
            app.GrabHolder_17 = uibutton(app.GridLayout2, 'state');
            app.GrabHolder_17.ValueChangedFcn = createCallbackFcn(app, @GrabHolder_ValueChanged, true);
            app.GrabHolder_17.Text = '17';
            app.GrabHolder_17.Layout.Row = 9;
            app.GrabHolder_17.Layout.Column = 1;

            % Create GrabHolder_18
            app.GrabHolder_18 = uibutton(app.GridLayout2, 'state');
            app.GrabHolder_18.ValueChangedFcn = createCallbackFcn(app, @GrabHolder_ValueChanged, true);
            app.GrabHolder_18.Text = '18';
            app.GrabHolder_18.Layout.Row = 9;
            app.GrabHolder_18.Layout.Column = 2;

            % Create GrabHolder_19
            app.GrabHolder_19 = uibutton(app.GridLayout2, 'state');
            app.GrabHolder_19.ValueChangedFcn = createCallbackFcn(app, @GrabHolder_ValueChanged, true);
            app.GrabHolder_19.Text = '19';
            app.GrabHolder_19.Layout.Row = 10;
            app.GrabHolder_19.Layout.Column = 1;

            % Create GrabHolder_20
            app.GrabHolder_20 = uibutton(app.GridLayout2, 'state');
            app.GrabHolder_20.ValueChangedFcn = createCallbackFcn(app, @GrabHolder_ValueChanged, true);
            app.GrabHolder_20.Text = '20';
            app.GrabHolder_20.Layout.Row = 10;
            app.GrabHolder_20.Layout.Column = 2;

            % Create GrabHolder_21
            app.GrabHolder_21 = uibutton(app.GridLayout2, 'state');
            app.GrabHolder_21.ValueChangedFcn = createCallbackFcn(app, @GrabHolder_ValueChanged, true);
            app.GrabHolder_21.Text = '21';
            app.GrabHolder_21.Layout.Row = 11;
            app.GrabHolder_21.Layout.Column = 1;

            % Create GrabHolder_22
            app.GrabHolder_22 = uibutton(app.GridLayout2, 'state');
            app.GrabHolder_22.ValueChangedFcn = createCallbackFcn(app, @GrabHolder_ValueChanged, true);
            app.GrabHolder_22.Text = '22';
            app.GrabHolder_22.Layout.Row = 11;
            app.GrabHolder_22.Layout.Column = 2;

            % Create GrabHolder_24
            app.GrabHolder_24 = uibutton(app.GridLayout2, 'state');
            app.GrabHolder_24.ValueChangedFcn = createCallbackFcn(app, @GrabHolder_ValueChanged, true);
            app.GrabHolder_24.Text = '24';
            app.GrabHolder_24.Layout.Row = 12;
            app.GrabHolder_24.Layout.Column = 2;

            % Create GrabHolder_12
            app.GrabHolder_12 = uibutton(app.GridLayout2, 'state');
            app.GrabHolder_12.ValueChangedFcn = createCallbackFcn(app, @GrabHolder_ValueChanged, true);
            app.GrabHolder_12.Text = '12';
            app.GrabHolder_12.Layout.Row = 6;
            app.GrabHolder_12.Layout.Column = 2;

            % Create GrabHolder_23
            app.GrabHolder_23 = uibutton(app.GridLayout2, 'state');
            app.GrabHolder_23.ValueChangedFcn = createCallbackFcn(app, @GrabHolder_ValueChanged, true);
            app.GrabHolder_23.Text = '23';
            app.GrabHolder_23.Layout.Row = 12;
            app.GrabHolder_23.Layout.Column = 1;

            % Create GrabHolder_25
            app.GrabHolder_25 = uibutton(app.GridLayout2, 'state');
            app.GrabHolder_25.ValueChangedFcn = createCallbackFcn(app, @GrabHolder_ValueChanged, true);
            app.GrabHolder_25.Text = '25';
            app.GrabHolder_25.Layout.Row = 13;
            app.GrabHolder_25.Layout.Column = 1;

            % Create GrabHolder_26
            app.GrabHolder_26 = uibutton(app.GridLayout2, 'state');
            app.GrabHolder_26.ValueChangedFcn = createCallbackFcn(app, @GrabHolder_ValueChanged, true);
            app.GrabHolder_26.Text = '26';
            app.GrabHolder_26.Layout.Row = 13;
            app.GrabHolder_26.Layout.Column = 2;

            % Create GrabHolder_27
            app.GrabHolder_27 = uibutton(app.GridLayout2, 'state');
            app.GrabHolder_27.ValueChangedFcn = createCallbackFcn(app, @GrabHolder_ValueChanged, true);
            app.GrabHolder_27.Text = '27';
            app.GrabHolder_27.Layout.Row = 14;
            app.GrabHolder_27.Layout.Column = 1;

            % Create GrabHolder_28
            app.GrabHolder_28 = uibutton(app.GridLayout2, 'state');
            app.GrabHolder_28.ValueChangedFcn = createCallbackFcn(app, @GrabHolder_ValueChanged, true);
            app.GrabHolder_28.Text = '28';
            app.GrabHolder_28.Layout.Row = 14;
            app.GrabHolder_28.Layout.Column = 2;

            % Create GrabHolder_29
            app.GrabHolder_29 = uibutton(app.GridLayout2, 'state');
            app.GrabHolder_29.ValueChangedFcn = createCallbackFcn(app, @GrabHolder_ValueChanged, true);
            app.GrabHolder_29.Text = '29';
            app.GrabHolder_29.Layout.Row = 15;
            app.GrabHolder_29.Layout.Column = 1;

            % Create GrabHolder_30
            app.GrabHolder_30 = uibutton(app.GridLayout2, 'state');
            app.GrabHolder_30.ValueChangedFcn = createCallbackFcn(app, @GrabHolder_ValueChanged, true);
            app.GrabHolder_30.Text = '30';
            app.GrabHolder_30.Layout.Row = 15;
            app.GrabHolder_30.Layout.Column = 2;

            % Create GrabHolder_31
            app.GrabHolder_31 = uibutton(app.GridLayout2, 'state');
            app.GrabHolder_31.ValueChangedFcn = createCallbackFcn(app, @GrabHolder_ValueChanged, true);
            app.GrabHolder_31.Text = '31';
            app.GrabHolder_31.Layout.Row = 16;
            app.GrabHolder_31.Layout.Column = 1;

            % Create GrabHolder_32
            app.GrabHolder_32 = uibutton(app.GridLayout2, 'state');
            app.GrabHolder_32.ValueChangedFcn = createCallbackFcn(app, @GrabHolder_ValueChanged, true);
            app.GrabHolder_32.Text = '32';
            app.GrabHolder_32.Layout.Row = 16;
            app.GrabHolder_32.Layout.Column = 2;
            
            
            app.BringbackholderButton.Position = [51 223 132 38];
            
            
            
            filterLabelHalfWidth=40;
            filterLabelHeight=20;
            filterLabelPad=25;
            panelMidPoint=109;
            % Create line above filter label
            app.BlackLine1 = uilabel(app.RightPanel);
            app.BlackLine1.BackgroundColor = [0 0 0];
            app.BlackLine1.FontSize = 1;
            app.BlackLine1.Position = [1 210 panelMidPoint*2 2];
            app.BlackLine1.Text = '-';

            %filterLabelPos = [panelMidPoint-filterLabelPad-2*filterLabelWidth panelMidPoint+filterLabelPad ]; %horizontal positions
            %for filter = 1:length(filterlabelPos) %length(app.grabHolderButts)

            %Filter1
            app.FilterLabel1 = uilabel(app.RightPanel);
            app.FilterLabel1.HorizontalAlignment = 'center';
            app.FilterLabel1.FontSize = 14;
            app.FilterLabel1.FontWeight = 'bold';
            app.FilterLabel1.Position = [panelMidPoint-filterLabelPad-2*filterLabelHalfWidth+15 185 2*filterLabelHalfWidth filterLabelHeight];
            app.FilterLabel1.Text = 'Filter 1';

            % Create FilterINButtons
            app.Filter1INButton = uibutton(app.RightPanel, 'push');
            app.Filter1INButton.ButtonPushedFcn = createCallbackFcn(app, @FilterINButtonPushed, true);
            app.Filter1INButton.Position = [panelMidPoint-filterLabelPad-2*filterLabelHalfWidth+15 160 30 22];
            app.Filter1INButton.Text = 'IN';

            % Create FilterOUTButtons
            app.Filter1OUTButton = uibutton(app.RightPanel, 'push');
            app.Filter1OUTButton.ButtonPushedFcn = createCallbackFcn(app, @FilterOUTButtonPushed, true);
            app.Filter1OUTButton.Position = [panelMidPoint-filterLabelPad-2*filterLabelHalfWidth+filterLabelHalfWidth+15 160 40 22];
            app.Filter1OUTButton.Text = 'OUT';
            

            %Filter2
            app.FilterLabel2 = uilabel(app.RightPanel);
            app.FilterLabel2.HorizontalAlignment = 'center';
            app.FilterLabel2.FontSize = 14;
            app.FilterLabel2.FontWeight = 'bold';
            app.FilterLabel2.Position = [panelMidPoint+filterLabelPad 185 2*filterLabelHalfWidth filterLabelHeight];
            app.FilterLabel2.Text = 'Filter 2';

            % Create FilterINButton
            app.Filter2INButton = uibutton(app.RightPanel, 'push');
            app.Filter2INButton.ButtonPushedFcn = createCallbackFcn(app, @FilterINButtonPushed, true);
            app.Filter2INButton.Position = [panelMidPoint+filterLabelPad 160 30 22];
            app.Filter2INButton.Text = 'IN';

            % Create FilterOUTButton
            app.Filter2OUTButton = uibutton(app.RightPanel, 'push');
            app.Filter2OUTButton.ButtonPushedFcn = createCallbackFcn(app, @FilterOUTButtonPushed, true);
            app.Filter2OUTButton.Position = [panelMidPoint+filterLabelPad+filterLabelHalfWidth 160 40 22];
            app.Filter2OUTButton.Text = 'OUT';

             % Create line below filters
            app.BlackLine3 = uilabel(app.RightPanel);
            app.BlackLine3.BackgroundColor = [0 0 0];
            app.BlackLine3.FontSize = 1;
            app.BlackLine3.Position = [1 145 panelMidPoint*2 2];
            app.BlackLine3.Text = '-';
           

            
            % Create LastCommandTextAreaLabel
            app.LastCommandTextAreaLabel = uilabel(app.RightPanel);
            app.LastCommandTextAreaLabel.HorizontalAlignment = 'center';
            app.LastCommandTextAreaLabel.Position = [10 110 61 28];
            app.LastCommandTextAreaLabel.Text = {'Last'; 'Command'};

            % Create LastCommandTextArea
            app.LastCommandTextArea = uitextarea(app.RightPanel);
            app.LastCommandTextArea.FontSize = 10;
            app.LastCommandTextArea.Position = [74 110 134 20];

            
            % Create Image
            app.Image = uiimage(app.RightPanel);
            app.Image.Position = [33 7 170 90];
            app.Image.ImageSource = 'C-ROBOT_v4.png';

           

            % Show the figure after all components are created
            app.UIFigure.Visible = 'on';
        end
    end

    % App creation and deletion
    methods (Access = public)

        % Construct app
        function app = Robot_GUI_exported

            % Create UIFigure and components
            createComponents(app)

            % Register the app with App Designer
            registerApp(app, app.UIFigure)

            % Execute the startup function
            runStartupFcn(app, @startupFcn)

            if nargout == 0
                clear app
            end
        end

        % Code that executes before app deletion
        function delete(app)

            % Delete UIFigure when app is deleted
            delete(app.UIFigure)
        end
    end
end