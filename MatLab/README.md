# MatLab tools

This contains the MatLab tools used to control the robot.
It is divided into `robotConnector.m` which is a class used to communicate with the robot over an IP network (Ethernet), and `Robot_GUI.mlapp` which is a GUI built on this.

Note that the binary `.mlapp` file the primary source file for the GUI; the file `Robot_GUI_exported.m` is included for convenience but is considered to be a compiled file, where any manual modifications will be lost the next time the `.mlapp` is exported.

## Serial comms
The matlab class can be used with serial-only, by setting the address to the appropriate device, e.g. `rob = robotConnector('/dev/ttyACM0')`.

Note that matlab requires to be able to create a lock file in `/var/lock/` aka `/run/lock/`. One way to do this is to

1. Add your user to the `lock` group on the computer: `sudo usermod -a -G lock YOUR_USERNAME_HERE` and log out/in

2. If needed, make the lock folder writable by the lock group:
```
cd /run
sudo chgrp lock lock
sudo chmod g+w lock
```
This was tested on Fedora 38.