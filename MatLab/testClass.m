
hostname = '192.168.1.177';

%Create class
disp(' ')
disp('*******************')
disp(' ')
disp(' Default callback  ');
disp(' ')
disp('*******************')
disp(' ')
conn = robotConnector(hostname);
disp(conn)
% ... initialization happens asynchronously ...
pause(5);
disp(conn)
clear conn;

%Create class with a self-defined callback
disp(' ')
disp('*******************')
disp(' ')
disp(' With callback (#1)');
disp(' ')
disp('*******************')
disp(' ')

callbackSimple = @(obj,stepper,grabber,temp) ...
     disp(['CallbackSimple: stepper=',num2str(stepper), ...
                         ', grabber=',num2str(grabber), ...
                         ', temp=',   num2str(temp) ]);

conn = robotConnector(hostname,callbackSimple);
disp(conn)
% ... initialization happens asynchronously ...
pause(5);
%During the pause, the callback function will be called
% as data is returning
clear conn;


%Callback (inline) that does disp(obj) after every update
disp(' ')
disp('*******************')
disp(' ')
disp(' With callback (#2)');
disp(' ')
disp('*******************')
disp(' ')

conn = robotConnector(hostname, @(obj,stepper,grabber,temp) disp(obj) );
disp(conn)
% ... initialization happens asynchronously ...
% Notice that the robotConnector object changes, in order to reflect the
% current status of the robot.
pause(5);
%During the pause, the callback function will be called
% as data is returning
clear conn;


%Second callback that also modifies the output of recieved text strings
% This can be used to redirect output to e.g. GUI.
disp(' ')
disp('*******************')
disp(' ')
disp(' With callback (#3)');
disp(' ')
disp('*******************')
disp(' ')
conn = robotConnector(hostname,@(obj,a,b,c) disp(obj), @(obj,text)disp(strcat('-> "', text,'"')));
disp(conn)
% ... initialization happens asynchronously ...
pause(5);
%During the pause, the callback function will be called
% as data is returning
clear conn;

