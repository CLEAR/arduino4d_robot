#! /usr/bin/env python3

#Small test script to send test packages
# to verify behaviour

import socket
import sys
import time

IP   = "192.168.1.31"
port = 23

buffsize = 4096;

def sendmessage(option):
    print ("Running message =",option)
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((IP,port));

    if option == 0:
        #Straight-forward single line command
        time.sleep(1);
        sock.sendall(b"123\n");
        time.sleep(1);
    elif option == 1:
        #Two line command
        time.sleep(1);
        sock.sendall(b"123\n456\n");
        time.sleep(1);
        
    elif option == 2:
        #Blank line
        time.sleep(1);
        sock.sendall(b"\n");
        time.sleep(1);
    elif option == 3:
        #Two blank lines
        time.sleep(1);
        sock.sendall(b"\n\n");
        time.sleep(1);
        
    elif option == 4:
        #Unfinished line
        time.sleep(1);
        sock.sendall(b"123");
        time.sleep(1);
    elif option == 5:
        #Late finished line
        time.sleep(1);
        sock.sendall(b"123");
        time.sleep(1);
        sock.sendall(b"\n");
        time.sleep(1);
    elif option == 6:
        #Hanging line
        time.sleep(1);
        sock.sendall(b"123\n456");
        time.sleep(1);
        sock.sendall(b"\n");
        time.sleep(1);

    elif option == 7:
        #Windows-endings
        time.sleep(1)
        sock.sendall(b"123\r\n")
        time.sleep(1);
    elif option == 8:
        #Windows-endings x2
        time.sleep(1)
        sock.sendall(b"123\r\n456\r\n")
        time.sleep(1);
    elif option == 9:
        #Windows-endings, late finish
        time.sleep(1)
        sock.sendall(b"123")
        time.sleep(1);
        sock.sendall(b"\r\n")
        time.sleep(1);
    elif option == 10:
        #Windows-endings, hanging
        time.sleep(1)
        sock.sendall(b"123\r\n456")
        time.sleep(1);
        sock.sendall(b"\r\n")
        time.sleep(1);

    elif option == 11:
        #Two immediate commands then delayable command, separate messages
        time.sleep(1)
        print("GRABBER OPEN")
        sock.sendall(b"GRABBER OPEN\n")
        time.sleep(5)
        print("GRABBER CLOSE")
        sock.sendall(b"GRABBER CLOSE\n")
        time.sleep(5)
        print("HELP")
        sock.sendall(b"HELP\n")
        time.sleep(1)
    elif option == 12:
        #Two immediate commands then delayed command, single buffer
        time.sleep(1)
        sock.sendall(b"GRABBER OPEN\nGRABBER CLOSE\nHELP\n")
        time.sleep(10)

    elif option == 13:
        #String is NULL
        time.sleep(1);
        sock.sendall(b"\0");
        time.sleep(1);
    elif option == 14:
        #Early NULL, at end of string
        time.sleep(1);
        sock.sendall(b"123\0");
        time.sleep(1);
    elif option == 15:
        #Early NULL, middle of string
        time.sleep(1);
        sock.sendall(b"123\0"+b"456");
        time.sleep(1);
    elif option == 16:
        #Early NULL, middle of line
        time.sleep(1);
        sock.sendall(b"123\0"+b"456\n");
        time.sleep(1);
    elif option == 17:
        #Early NULL, just after newline
        time.sleep(1);
        sock.sendall(b"123\n\0");
        time.sleep(1);

    elif option == 18:
        #Immediate, then a line with NULL in the middle
        time.sleep(1);
        sock.sendall(b"GRABBER OPEN\nGRABBER CLOSED\n\0"+b"456\n");
        time.sleep(10);
        
    else:
        print("Unknown option:", option)

    buff = sock.recv(buffsize);
    print("Buffer:\n'"+str(buff)+"'");
    #TODO:
    # EARLY NULL, WITH AND WITHOUT IMMEDIATE
    # IMMEDIATE + WINDOWS
    
    sock.close()


if len(sys.argv)==2:
    option = int(sys.argv[1])
    sendmessage(option)
else:
    for i in range(0,4):
        sendmessage(i)
        time.sleep(1)
