clear all
clc
close all

a = arduino();
s = servo(a, 'D9');
writePosition(s, 0);
for angle = 0:0.1:0.9
    writePosition(s, angle);
    current_pos = readPosition(s);
    pause(1);
end
pause(2);
for angle = 0.9:-0.1:0
    writePosition(s, angle);
    current_pos = readPosition(s);
    pause(1);
end