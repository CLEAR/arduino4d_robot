clear all
clc
close all

a = arduino();

%% PinDefinition
%Mot1
SwitchXB = 'D2';
SwitchXA = 'D3';
DIRX     = 'D32';
STEPX    = 'D33';
%Mot2
SwitchYB = 'D5';
SwitchYA = 'D4';
DIRY     = 'D36';
STEPY    = 'D37';
%Mot3
SwitchZB = 'D7';
SwitchZA = 'D6';
DIRZ     = 'D40';
STEPZ    = 'D41';

%% VariableDefinition
pd        = 5e-3; % Speed     -- Lower gose faster
setdir    = 0;     % Direction -- LOW   the motor - HIGH 2 opposite
goX       = 1;     % LOW stop - HIGH go -- X-PLANE
goY       = 0;      % LOW stop - HIGH go -- Y-PLANE
goZ       = 0;     % LOW stop - HIGH go -- Z-PLANE
Stage     = 1;     % 1--> X 2--> Y 3--> Z
inibition = 0;     % LOW All can work - HIGH Nothing will work



switch (Stage)
    case 1
        if (goX == 1) 
            go      = goX; 
            DIR     = DIRX; 
            STEP    = STEPX; 
            SwitchA = SwitchXA; 
            SwitchB = SwitchXB; 
        end
    case 2
        if (goY == 1) 
            go      = goY;
            DIR     = DIRY; 
            STEP    = STEPY; 
            SwitchA = SwitchYA; 
            SwitchB = SwitchYB; 
        end
    case 3
        if (goZ == 1) 
            go      = goZ;
            DIR     = DIRZ; 
            STEP    = STEPZ; 
            SwitchA = SwitchZA; 
            SwitchB = SwitchZB; 
        end
    otherwise
        error("WRONG MOTORS!");
end

%% Initialization
configurePin(a,DIR,'DigitalOutput');
configurePin(a,STEP,'DigitalOutput');
configurePin(a,SwitchA,'DigitalInput');
configurePin(a,SwitchB,'DigitalInput');

%% RunningCode
while (true)
    SwitchBval = readDigitalPin(a, SwitchB);
    SwitchAval = readDigitalPin(a, SwitchA);

    if ((SwitchAval==0 && setdir==0)|| (SwitchBval==0 && setdir==1))
        go = 0;
    else
        go = 1;
    end
    if (inibition == 0)
        if (go == 1)
            RunMotor(a,DIR,STEP,setdir,pd);
        end
    end
end

%% MotorMovement
function RunMotor(a,DIR,STEP,setdir,pd) 
    writeDigitalPin(a,DIR,setdir);
    writeDigitalPin(a,STEP,1);
    pause(pd);
    writeDigitalPin(a,STEP,0);
    pause(pd);
end