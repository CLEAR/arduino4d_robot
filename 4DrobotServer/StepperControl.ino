/* 
 * This file is part of 4DrobotServer.
 * 4DrobotServer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 4DrobotServer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with 4DrobotServer. If not, see <https://www.gnu.org/licenses/>. 
 */

void setup_stepper() {
  // Set pin modes
  for (size_t i=0; i<stepper_numAxis; i++) {
    pinMode(stepper_dir_pin[i], OUTPUT);
    pinMode(stepper_step_pin[i], OUTPUT);
    
    pinMode(stepper_switchMAX_pin[i], INPUT);
    pinMode(stepper_switchMIN_pin[i], INPUT);
  }

  //Compute the acceleration profile
  // Based on https://www.microchip.com/en-us/application-notes?rv=1234ab46
  // For us, alpha=1 [radian per step] because we care about steps not radians,
  // and we want to compute the t_{n+1}-t_n
  float stepper_accel_sqrt = (sqrt(stepper_accelerate_steps+1)-sqrt(stepper_accelerate_steps)) * sqrt(2.0)/stepper_delay_fast;
  float stepper_accel_aux = sqrt(2.0)/stepper_accel_sqrt;

  #ifdef ACCELERATION_DEBUG
  Serial.print(F("@ stepper_accelerate_steps="));
  Serial.println(stepper_accelerate_steps);
  Serial.print(F("@ stepper_delay_fast="));
  Serial.println(stepper_delay_fast);
  Serial.print(F("@ stepper_accel_sqrt="));
  Serial.println(stepper_accel_sqrt,6);
  Serial.print(F("@ stepper_accel_aux="));
  Serial.println(stepper_accel_aux,6);
  #endif

  if (stepper_delay_slow < stepper_delay_fast) {
    Serial.print(F("@ stepper_delay_slow = "));
    Serial.print(stepper_delay_slow);
    Serial.print(F(" < stepper_delay_fast = "));
    Serial.println(stepper_delay_fast);
    Serial.println(F("@ This is not acceptable"));
    while (true) {
      // Do nothing
      delay(1);
    }
  }
  stepper_accelerate_steps_slow = 0;

  for (size_t i=0; i < stepper_accelerate_steps; i++) {
    stepper_accel_profile[i] = stepper_accel_aux*(sqrt(i+1)-sqrt(i));

    #ifdef ACCELERATION_DEBUG
    Serial.print(F("@ "));
    Serial.print(F("i="));
    Serial.print(i);
    Serial.print(F(" delay="));
    Serial.print(stepper_accel_profile[i]);
    Serial.print(" (");
    Serial.print(stepper_accel_aux*(sqrt(i+1)-sqrt(i)),6);
    Serial.println(")");
    #endif

    if (stepper_accel_profile[i] > stepper_delay_slow) {
      stepper_accelerate_steps_slow = i+1; // Length of array to use
    }
  }

  if (stepper_accelerate_steps_slow == 0) {
    Serial.println(F("@ Warning: stepper_accel_profile[0] <= stepper_delay_slow"));
    Serial.println(F("@ There will be no acceleration to slow speed."));
  }
  #ifdef ACCELERATION_DEBUG
  Serial.print(F("@ stepper_delay_slow="));
  Serial.println(stepper_delay_slow);
  Serial.print(F("@ stepper_accelerate_steps_slow="));
  Serial.println(stepper_accelerate_steps_slow);
  #endif

  if (stepper_delay_fast < stepper_onTime) {
    // We will subtract `stepper_onTime` from the `stepper_delay`s, and this should not be negative...
    Serial.println(F("@ stepper_delay_fast < stepper_onTime.  Sorry, I can't move that fast..."));
    while (true) {
      // Do nothing
      delay(1);
    }
  }
}

void stepper_control() {
  if(not stepper_go) {
    return;
  }

  //Initialize limit switch variables and check for wiring issue
  bool onMAX = (digitalRead(stepper_switchMAX_pin[stepper_go_axis]) == stepper_switch_active);
  bool onMIN = (digitalRead(stepper_switchMIN_pin[stepper_go_axis]) == stepper_switch_active);
  if (onMAX and onMIN) {
    bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< STEPPER ERROR IMPOSSIBLE LIMIT SWITCH STATUS, BOTH ARE ACTIVE. PLEASE CHECK WIRING.\n"));

    input_queue_clear();
    goto resetGO;
  }

  //Check the XY interlock
  // Produce an error if we try to move locked axis
  if( (stepper_go_axis != STEPPER_Z) and                   //If the axis we are moving is not Z, check further...
      (
        (not stepper_mode_absolute[STEPPER_Z]) or          //If Z is not in absolute mode, STOP
        (stepper_pos[STEPPER_Z] >= stepper_Zpos_unlockXY)  //If Z pos is above the "unsafe ceiling", STOP
                                                           // Note: The Z axis is phsyically pointing DOWN,
                                                           //       so this means we are not near the top.
      )
    )

    {
    //Yes, we are trying to move a locked axis.

    // Is the interlock disabled?
    if (stepper_interlock) {
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< STEPPER ERROR XY INTERLOCK\n"));

      input_queue_clear();
      goto resetGO;
    }
    else {
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< STEPPER WARNING XY INTERLOCK IGNORED\n"));
    }
  }

  //Move!
  bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< STEPPER MOVING "));
  bufferWrite(output_buff, sizeof(output_buff), output_buffCount, stepper_axnames[stepper_go_axis]);
  bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("...\n"));
  output_buff_flush();

//  noInterrupts(); //Disable interrupts in order to reduce jitter
                  // from ~1 us to less than what I could measure with Picoscope 4262 / 24 pulses window
                  // One issue is that serial inputs will be partially ignored during this time
                  // (Ethernet inputs will be fine)

  //Task: Zeroseek an axis
  if (stepper_go_zero) {
    //If we are zeroseeking, we do no longer trust the current zero position
    stepper_mode_absolute[stepper_go_axis] = false;
    
    //Always move in negative direction when zeroing
    digitalWrite(stepper_dir_pin[stepper_go_axis], stepper_backward[stepper_go_axis]);

    //Move until we trigger the low interlock switch
    int  onLimitCount_towards = 0L;
    int  onLimitCount_away    = 0L;

    long int stepper_acc_idx = 0;
    long int stepper_startPos = stepper_pos[stepper_go_axis];

    bool finishedZeroseek = false;
    while (not finishedZeroseek) {
      if (digitalRead(emergency_stop_pin) == emergency_isEmergency) {
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< STEPPER ERROR EMERGENCY STOP\n"));

        input_queue_clear();
        goto resetGO;
      }
      if (onMIN) {
        //Avoid repeatedly moving `stepper_onlimit_steps_towards` into the expected limit switch
        // with repeaded ZEROSEEKS without other movement on this axis.
        // This could cause damage and misaligned movement.
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< STEPPER ZEROSEEK ALREADY ON MIN LIMIT SWITCH\n"));
      }
      else {
        while (true) {
          if (digitalRead(emergency_stop_pin) == emergency_isEmergency) {
            bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< STEPPER ERROR EMERGENCY STOP\n"));

            input_queue_clear();
            goto resetGO;
          }
    
          //Check the other-end switch that we aren't moving in the wrong direction or stuck
          if(onMAX) {
            onLimitCount_away++;
          }
          if (onMIN) {
            onLimitCount_towards++;
          }
    
          if(onLimitCount_away > stepper_onlimit_steps_away) {
            //Hit the wrong limit switch too many times
            bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< STEPPER ERROR LIMIT SWITCH"));
            bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F(" MIN = "));
            bufferWrite(output_buff, sizeof(output_buff), output_buffCount, onMIN?"1":"0");
            bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F(" MAX = "));
            bufferWrite(output_buff, sizeof(output_buff), output_buffCount, onMAX?"1":"0");
            bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F(" (WAS MOVING AWAY FROM TRIGGERED SWITCH)\n"));

            input_queue_clear();
            goto resetGO;
          }
          if(onLimitCount_towards > 0) {
            //Hit the expected limit switch
            break;
          }
    
          //Move (slowly)
          digitalWrite(stepper_step_pin[stepper_go_axis],HIGH);
          delayMicroseconds(stepper_onTime);
          digitalWrite(stepper_step_pin[stepper_go_axis],LOW);
    
          unsigned int do_steps = stepper_delay_slow - stepper_onTime;
    
          if (stepper_acc_idx < stepper_accelerate_steps_slow) {
    
            #ifdef ACCELERATION_DEBUG
            Serial.print(F("@ Up: Interval#"));
            Serial.print(stepper_pos[stepper_go_axis]);
            Serial.print(F(" delay#"));
            Serial.print(stepper_acc_idx);
            Serial.print(F(" delay="));
            Serial.println(stepper_accel_profile[stepper_acc_idx]);
            #endif
    
            do_steps = stepper_accel_profile[stepper_acc_idx] - stepper_onTime;
    
            stepper_acc_idx += 1;
          }
          #ifdef ACCELERATION_DEBUG
          else {
            #ifdef ACCELERATION_DEBUG
            Serial.print(F("@ Mn: Interval#"));
            Serial.print(stepper_pos[stepper_go_axis]);
            Serial.print(F(" delay#N/A delay="));
            Serial.println(stepper_delay_slow);
            #endif
          }
          #endif
    
          while (do_steps > stepper_maxwait_us) {
            delayMicroseconds(stepper_maxwait_us);
            do_steps -= stepper_maxwait_us;
          }
          delayMicroseconds(do_steps);
    
          stepper_pos[stepper_go_axis] -= 1L;

          if (abs(stepper_pos[stepper_go_axis]) >= stepper_maxsteps_move[stepper_go_axis]) {
            bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< STEPPER ERROR MAXSTEPS_MOVE\n"));

            input_queue_clear();
            goto resetGO;
          }
  
          //Poll switches
          onMAX = (digitalRead(stepper_switchMAX_pin[stepper_go_axis]) == stepper_switch_active);
          onMIN = (digitalRead(stepper_switchMIN_pin[stepper_go_axis]) == stepper_switch_active);
        }
      }
  
      //Confirm limit switch
      int hits = 0;
      for (hits = 0; hits < stepper_zeroseek_confirmHitCounts; hits++) {
          unsigned int do_steps = stepper_zeroseek_confirmHitDelay;
          while (do_steps > stepper_maxwait_us) {
            delayMicroseconds(stepper_maxwait_us);
            do_steps -= stepper_maxwait_us;
          }
          delayMicroseconds(do_steps);
  
          onMAX = (digitalRead(stepper_switchMAX_pin[stepper_go_axis]) == stepper_switch_active);
          onMIN = (digitalRead(stepper_switchMIN_pin[stepper_go_axis]) == stepper_switch_active);
  
          if (!onMIN or onMAX) {
            bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< STEPPER WARNING LIMIT SWITCH INCONSISTENCY WHEN VERIFYING ZEROSEEK COMPLETENESS"));
            bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F(" MIN="));
            bufferWrite(output_buff, sizeof(output_buff), output_buffCount, onMIN?"1":"0");
            bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F(", MAX="));
            bufferWrite(output_buff, sizeof(output_buff), output_buffCount, onMAX?"1":"0");
            bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F(" EXPECTED MIN=1, MAX=0\n"));

            bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< STEPPER RETRYING ZEROSEEK "));
            bufferWrite(output_buff, sizeof(output_buff), output_buffCount, stepper_axnames[stepper_go_axis]);
            bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F(" ...\n"));
            output_buff_flush();

            //Reset and keep moving...
            onMAX = (digitalRead(stepper_switchMAX_pin[stepper_go_axis]) == stepper_switch_active);
            onMIN = (digitalRead(stepper_switchMIN_pin[stepper_go_axis]) == stepper_switch_active);
            onLimitCount_towards = 0L;
            onLimitCount_away    = 0L;

            finishedZeroseek = false;
            break;
          }
      }
      if (hits == stepper_zeroseek_confirmHitCounts) {
        //We're good!
        finishedZeroseek = true;
      }

    }
    bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< STEPPER ZEROSEEK MOVED "));
    char buff[13];
    snprintf(buff,sizeof(buff), "%+011ld\n",stepper_startPos-stepper_pos[stepper_go_axis]);
    bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff);

    //Movement to switch completed successfully,
    stepper_pos[stepper_go_axis]           = 0L;
    stepper_mode_absolute[stepper_go_axis] = true;
  }

  //Task: Move to given position (either relative or absolute)
  else {
    int stepping = 0;
    if (stepper_goto > stepper_pos[stepper_go_axis]) {
      digitalWrite(stepper_dir_pin[stepper_go_axis], stepper_forward[stepper_go_axis]);
      stepping = 1;
    }
    else if(stepper_goto < stepper_pos[stepper_go_axis]) {
      digitalWrite(stepper_dir_pin[stepper_go_axis], stepper_backward[stepper_go_axis]);
      stepping = -1;
    }
    else {
      // Else not strictly needed:
      // If they are equal, the while loop doesn't iterate.
      goto resetGO;
    }

    //Move until goto==pos or we trigger an interlock switch stepper_onlimit_steps times
    int  onLimitCount_towards = 0L;
    int  onLimitCount_away    = 0L;

    // Aux variables for acceleration
    size_t stepper_accelerate_steps_use=0; //Max length of accel profile array to use
    unsigned int stepper_delay_flatTop = 0;
    if (stepper_mode_absolute[stepper_go_axis]) {
      //Go fast!
      stepper_accelerate_steps_use = stepper_accelerate_steps;
      stepper_delay_flatTop = stepper_delay_fast;
    }
    else {
      //Go slow.
      stepper_accelerate_steps_use = stepper_accelerate_steps_slow;
      stepper_delay_flatTop = stepper_delay_slow;
    }
    
    long int stepper_start = stepper_pos[stepper_go_axis];
    long int moveLength = stepper_goto-stepper_start;
    long int stepper_acc = abs(moveLength)/2; //Actual number of acceleration delays
    bool stepper_triangular = true;
    if (stepper_acc > stepper_accelerate_steps_use) {
      //Long movement -- limit acceleration steps to the max
      stepper_acc = stepper_accelerate_steps_use;
      stepper_triangular = false;
    }
    long int stepper_acc_idx = 0;
    #ifdef ACCELERATION_DEBUG
    Serial.print(F("@ stepper_acc="));
    Serial.println(stepper_acc);
    Serial.print(F("@ stepper_delay_flatTop="));
    Serial.println(stepper_delay_flatTop);
    Serial.print(F("@ stepper_triangular="));
    Serial.println(stepper_triangular);
    #endif

    while (true) {
      if (digitalRead(emergency_stop_pin) == emergency_isEmergency) {
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< STEPPER ERROR EMERGENCY STOP\n"));

        input_queue_clear();
        goto resetGO;
      }

      //Check limit switches
      onMAX = (digitalRead(stepper_switchMAX_pin[stepper_go_axis]) == stepper_switch_active);
      onMIN = (digitalRead(stepper_switchMIN_pin[stepper_go_axis]) == stepper_switch_active);

      if(onMAX) {
        if (stepping==1) {
          onLimitCount_towards++;
        }
        else if (stepping==-1) {
          onLimitCount_away++;
        }
      }

      if(onMIN) {
        if (stepping==-1) {
          onLimitCount_towards++;
        }
        else if (stepping==1) {
          onLimitCount_away++;
        }
      }

      if(onLimitCount_towards > stepper_onlimit_steps_towards) {
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< STEPPER ERROR LIMIT SWITCH"));
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F(" MIN = "));
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, onMIN?"1":"0");
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F(" MAX = "));
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, onMAX?"1":"0");
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F(" (WAS MOVING TOWARDS TRIGGERED SWITCH)\n"));

        input_queue_clear();
        goto resetGO;
      }
      if(onLimitCount_away > stepper_onlimit_steps_away) {
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< STEPPER ERROR LIMIT SWITCH"));
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F(" MIN = "));
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, onMIN?"1":"0");
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F(" MAX = "));
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, onMAX?"1":"0");
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F(" (WAS MOVING AWAY FROM TRIGGERED SWITCH)\n"));

        input_queue_clear();
        goto resetGO;
      }

      //Pulse for movement
      digitalWrite(stepper_step_pin[stepper_go_axis],HIGH);
      delayMicroseconds(stepper_onTime);
      digitalWrite(stepper_step_pin[stepper_go_axis],LOW);

      //Count!
      stepper_pos[stepper_go_axis] += stepping;
      
      if (abs(stepper_pos[stepper_go_axis]) >= stepper_maxsteps_move[stepper_go_axis]) {
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< STEPPER ERROR MAXSTEPS_MOVE\n"));

        input_queue_clear();
        goto resetGO;
      }

      if (stepper_pos[stepper_go_axis] == stepper_goto) {
        //Done stepping
        goto resetGO;
      }

      //Delay before next pulse
      if (abs(stepper_pos[stepper_go_axis]-stepper_start) <= stepper_acc) { //Ramp-up

        #ifdef ACCELERATION_DEBUG
        Serial.print(F("@ Up: Interval#"));
        Serial.print(stepper_pos[stepper_go_axis]);
        Serial.print(F("@  delay#"));
        Serial.print(stepper_acc_idx);
        Serial.print(F("@  delay="));
        Serial.println(stepper_accel_profile[stepper_acc_idx]);
        #endif

        unsigned int do_steps = stepper_accel_profile[stepper_acc_idx] - stepper_onTime;
        while (do_steps > stepper_maxwait_us) {
          delayMicroseconds(stepper_maxwait_us);
          do_steps -= stepper_maxwait_us;
        }
        delayMicroseconds(do_steps);

        stepper_acc_idx +=1;
      }
      else if (abs(stepper_pos[stepper_go_axis]-stepper_goto) <= stepper_acc) { //Ramp-down
        if (stepper_acc_idx == stepper_acc && stepper_triangular && abs(stepper_goto-stepper_start)%2 == 0) {
          //Handle triangular (not trapezoidal) velocity profiles without flat-top,
          // with odd number of intervals (=>even number of steps)
          stepper_acc_idx--;
          #ifdef ACCELERATION_DEBUG
          Serial.println(F("@ subtracted 1"));
          #endif
        }

        unsigned int do_steps = stepper_accel_profile[--stepper_acc_idx]  - stepper_onTime;
        while (do_steps > stepper_maxwait_us) {
          delayMicroseconds(stepper_maxwait_us);
          do_steps -= stepper_maxwait_us;
        }
        delayMicroseconds(do_steps);

        #ifdef ACCELERATION_DEBUG
        Serial.print(F("@ Dn: Interval#"));
        Serial.print(stepper_pos[stepper_go_axis]);
        Serial.print(F("@  delay#"));
        Serial.print(stepper_acc_idx);
        Serial.print(F("@  delay="));
        Serial.println(stepper_accel_profile[stepper_acc_idx]);
        #endif
      }
      else { //Flat-top
        unsigned int do_steps = stepper_delay_flatTop - stepper_onTime;
        while (do_steps > stepper_maxwait_us) {
          delayMicroseconds(stepper_maxwait_us);
          do_steps -= stepper_maxwait_us;
        }
        delayMicroseconds(do_steps);

        #ifdef ACCELERATION_DEBUG
        Serial.print(F("@ Mn: Interval#"));
        Serial.print(stepper_pos[stepper_go_axis]);
        Serial.print(F("@  delay#N/A delay="));
        Serial.println(stepper_delay_flatTop);
        #endif
      }
    } // END while(true)
  } // END if(stepper_go_zero) / ELSE
    

  //Reset the flags which tells it where to go
  //Can also jump (GOTO) here directly on error
  resetGO: //Yep this is a GOTO label.
           // Please read XKCD297 & XKCD292

//  interrupts(); //Re-enable Arduino interupts,
                // i.e. continue normal operation

  digitalWrite(stepper_step_pin[stepper_go_axis],LOW);
  digitalWrite(stepper_dir_pin[stepper_go_axis], LOW);

  //Message '< STEPPER GO FINISHED AXIS <X/Y/Z> POS <POSITION>\n'
  bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< STEPPER GO FINISHED AXIS "));
  bufferWrite(output_buff, sizeof(output_buff), output_buffCount, stepper_axnames[stepper_go_axis]);
  bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F(" POS "));
  char buff[13];
  snprintf(buff,sizeof(buff), "%+011ld\n",stepper_pos[stepper_go_axis]);
  bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff);
  output_buff_flush();

  stepper_go      = false;
  stepper_go_zero = false;
  stepper_go_axis = 0;
  stepper_goto    = 0L;

  return;
}
