/*
 * This file is part of 4DrobotServer.
 * 4DrobotServer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 4DrobotServer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with 4DrobotServer. If not, see <https://www.gnu.org/licenses/>.
 */

void setup_networkIO() {
  //Network setup
  Serial.print("@ INFO BOOT Starting networking...\n");
  
  #ifdef NO_ETHERNET
  
  Serial.print("@ INFO BOOT Noticing that NO_ETHERNET is enabled. Entering serial-only mode.\n");
  
  #else
  
  #ifdef USE_DHCP
  Ethernet.begin(mac);
  #else
  Ethernet.begin(mac, ip, myDns, gateway, subnet);
  #endif

  // Check for Ethernet hardware present
  if (Ethernet.hardwareStatus() == EthernetNoHardware) {
    Serial.println("@ INFO BOOT Ethernet shield was not found.  Sorry, can't run without hardware when NO_ETHERNET is not enabled.");
    while (true) {
      // Do nothing, no point running without Ethernet hardware
      // TODO: Not actually true, we could also run with just serial...
      delay(1);
    }
  }
  while(true)
    if (Ethernet.linkStatus() == LinkOFF) {
      Serial.print("@ INFO BOOT Ethernet cable is not connected\n");
    }
    else {
      Serial.print("@ INFO BOOT Ethernet cable connected / status unknown\n");
      break;
  }
  byte buffMAC[6];
  Serial.print("@ INFO BOOT MAC address:     ");
  Ethernet.MACAddress(buffMAC);
  for (byte octet=0; octet<6;octet++) {
    Serial.print(buffMAC[octet],HEX);
    if(octet < 5) {
      Serial.print(":");
    }
  }
  Serial.print('\n');
  Serial.print("@ INFO BOOT IP address:      "); Serial.print(Ethernet.localIP());     Serial.print('\n');
  Serial.print("@ INFO BOOT DNS address:     "); Serial.print(Ethernet.dnsServerIP()); Serial.print('\n');
  Serial.print("@ INFO BOOT Gateway address: "); Serial.print(Ethernet.gatewayIP());   Serial.print('\n');
  Serial.print("@ INFO BOOT Subnet:          "); Serial.print(Ethernet.subnetMask());  Serial.print('\n');
  //Server setup
  telnet_socket.begin();

  #endif

  for (uint8_t i=0; i<telnet_numConnections; i++) {
    memset(telnet_buff[i], '\0', sizeof(telnet_buff[i]));
    telnet_buffCount[i] = 0;
    telnet_buffCount_prev[i] = 1;
  }

  memset(serial_buff, '\0', sizeof(serial_buff));
  serial_buffCount = 0;
  //  Starts at 1 to trigger on the first round.
  serial_buffCount_prev = 1;

  memset(output_buff, '\0', sizeof(output_buff));
  output_buffCount = 0;

}

void telnet_server() {
  // Program for running the telnet- and serial communication

  //1. Check for new connections
  #ifndef NO_ETHERNET
  while (true) {
    //Loop in case there are multiple new connections
    EthernetClient newConnection = telnet_socket.accept();
    if (newConnection) {
      //Find a new connection slot
      for (size_t i=0; i<telnet_numConnections; i++) {
        if (!telnet_connection[i]) {
          telnet_connection[i] = newConnection;

          bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< INFO CONN MAKE: "));

          IPAddress IP = telnet_connection[i].remoteIP();
          char IP_buff[16];
          snprintf(IP_buff, sizeof(IP_buff), "%d.%d.%d.%d", IP[0],IP[1],IP[2],IP[3]);
          bufferWrite(output_buff, sizeof(output_buff), output_buffCount, IP_buff);

          snprintf(IP_buff, sizeof(IP_buff), " ON #%-2d\n", i);
          bufferWrite(output_buff, sizeof(output_buff), output_buffCount, IP_buff);

          telnet_connection[i].print(F("@ INFO MSG Say 'HELP' to get started\n"));

          break;
        }
      }
    }
    else {
      break;
    }
  }
  #endif

  //2. Check for incoming data (telnet & serial) and buffer it
  #ifndef NO_ETHERNET
  for (size_t i = 0; i < telnet_numConnections; i++) {
    while (telnet_connection[i]             &&
           telnet_connection[i].connected() &&
           telnet_connection[i].available()  > 0   ) {
      bufferWrite(telnet_buff[i], sizeof(telnet_buff[i]), telnet_buffCount[i], telnet_connection[i].read());

      #ifdef PARSER_DEBUG
      Serial.print(F("@ Got '"));
      Serial.print(telnet_buff[i][telnet_buffCount[i]-1]);
      Serial.print(F("' = "));
      Serial.print((byte)telnet_buff[i][telnet_buffCount[i]-1]);
      Serial.print(F(", pos = "));
      Serial.print(telnet_buffCount[i]-1);
      Serial.print(F(", connection = "));
      Serial.println(i);
      #endif
    }
  }
  #endif
  while (Serial.available() > 0) {
    bufferWrite(serial_buff, sizeof(serial_buff), serial_buffCount, Serial.read());

    #ifdef PARSER_DEBUG
    Serial.print(F("@ Got '"));
    Serial.print(serial_buff[serial_buffCount-1]); //Note: the @'ing doesn't work perfectly when the character is a '\n'
    Serial.print(F("' = "));
    Serial.print((byte)serial_buff[serial_buffCount-1]);
    Serial.print(F(", pos = "));
    Serial.print(serial_buffCount-1);
    Serial.print(F(", connection = "));
    Serial.println(F("SERIAL"));
    #endif
  }

  //3. Feed the clients & serial with the content of the output buffer
  output_buff_flush();

  //4. Parse input buffers & notify that we are ready for more
  bool keepParsing = true;
  bool telnet_printReady[telnet_numConnections];
  bool serial_printReady = false;
  #ifndef NO_ETHERNET
  for (size_t i = 0; (i < telnet_numConnections); i++) {
    if (not (telnet_connection[i] &&
             telnet_connection[i].connected()
            ) ) {
      continue;
    }
    telnet_printReady[i] = false;
    if (keepParsing) {
      if (telnet_buffCount[i] == 0 and telnet_buffCount_prev[i]>0) {
        telnet_printReady[i] = true;
      }
      telnet_buffCount_prev[i] = telnet_buffCount[i];

      keepParsing = input_parser(telnet_buff[i], sizeof(telnet_buff[i]), telnet_buffCount[i], &(telnet_connection[i]));
    }
  }
  #endif
  if (keepParsing) {
    if (serial_buffCount == 0 and serial_buffCount_prev>0) {
      serial_printReady = true;
    }
    serial_buffCount_prev = serial_buffCount;

    input_parser(serial_buff, sizeof(serial_buff), serial_buffCount, &(Serial));
  }

  //5. Feed the clients & serial with the content of the output buffer generated during parsing
  output_buff_flush();

  //5b. Notify client that TTY is ready for more
  #ifndef NO_ETHERNET
  for (size_t i = 0; i < telnet_numConnections; i++) {
    if (not (telnet_connection[i] &&
             telnet_connection[i].connected()
            ) ) {
      continue;
    }
    if (telnet_printReady[i]) {
      telnet_connection[i].print("$\n");
    }
  }
  #endif
  if(serial_printReady) {
    Serial.print("$\n");
  }

  //6. Check for telnet disconnects & handle
  #ifndef NO_ETHERNET
  for (size_t i = 0; i < telnet_numConnections; i++) {
    if ( telnet_connection[i] &&
        !telnet_connection[i].connected() ) {

      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< INFO CONN BREAK: "));

      IPAddress IP = telnet_connection[i].remoteIP();
      char IP_buff[16];
      snprintf(IP_buff, sizeof(IP_buff), "%d.%d.%d.%d", IP[0],IP[1],IP[2],IP[3]);
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, IP_buff);

      snprintf(IP_buff, sizeof(IP_buff), " ON #%-2d\n", i);
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, IP_buff);

      telnet_connection[i].stop();

      memset(telnet_buff[i],'\0',sizeof(telnet_buff[i]));
      telnet_buffCount[i]=0;
      telnet_buffCount_prev[i]=1;
    }
  }
  #endif
  //7. Feed the clients & serial with disconnect messages
  output_buff_flush();
}

void output_buff_flush() {
  //Call this function to send the output data in the queue
  // to all clients (TCP/IP and Serial)

  if(output_buffCount) {
    #ifndef NO_ETHERNET
    for (size_t i = 0; i < telnet_numConnections; i++) { 
      if (telnet_connection[i] &&
          telnet_connection[i].connected() ) {
        telnet_connection[i].print(output_buff);
      }
    }
    #endif
    
    Serial.print(output_buff);
    Serial.flush();

    memset(output_buff, '\0', sizeof(output_buff));
    output_buffCount = 0;
  }
}

void input_queue_clear(){
  //Clear all the commands in the input queues, and return to ready state
  // i.e. after a limit switch hit, don't keep executing commands when in an unexpected position...

  output_buff_flush();

  bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("@ ERROR HAS OCCURED, CLEARING ALL INPUT BUFFERS NOW\n"));
  bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("@ INFO AWAITING NEW ORDERS\n"));
  output_buff_flush();

  for (size_t i = 0; i < telnet_numConnections; i++) {
    memset(telnet_buff[i],'\0',sizeof(telnet_buff[i]));
    telnet_buffCount[i]=0;
  }

  memset(serial_buff,'\0',sizeof(serial_buff));
  serial_buffCount = 0;
}

bool input_parser(char* in, const size_t buffSize, size_t& buffCount, Stream* originatingStream) {
  // Attempt to parse the commands in the buffer.
  // It will only attempt to parse commands that end with a '\n', '\r', or a combination thereof
  // Return true to continue parsing lines from the next input buffer,
  // or false to tell telnet_server() to stop parsing now,
  // do the cleanup, and let loop() iterate to act on the command that was recieved.

  //1. Sanity-check the buffer size
  // (Note: Last entry is always a \0, buffSize cannot point to it)
  if (buffCount == buffSize-1) {
    output_buff_flush();
    originatingStream->print(F("@ INFO BUFFER FULL\n"));
    originatingStream->print(F("@ INFO BUFFER RESET\n"));

    memset(in,'\0', buffSize);
    buffCount=0;

    return true;
  }

  //2. Look for whole lines & parse each of them in order
  char line_buff[buffSize];
  memset(line_buff,'\0', sizeof(line_buff));
  size_t line_next = 0; //Index of the first valid character

  bool lastchar_was_newline = true;
  bool gotCommand           = false; //Print a `$` when done, after having run commands

  bool keepParsing          = true;  //Switches to false if a command we need to treat
                                     // *right now* comes through (i.e. a motor movement)

  for (size_t i = 0; (i<buffCount); i++) {

    if(in[i] == '\0') {
      //Sanity check: There should be no NULL before the end of the buffer `in`

      output_buff_flush();

      #ifdef PARSER_DEBUG
      Serial.println(F("@ Buffer contains NULL"));
      #endif

      originatingStream->print(F("@ INFO BUFFER NULL"));
      originatingStream->print(F("@ INFO BUFFER RESET"));

      memset(line_buff,'\0', sizeof(line_buff));
      memset(in,'\0', buffSize);
      line_next = 0;
      buffCount = 0;

      return keepParsing;
    }
    else if (lastchar_was_newline) {
      //Skip repeated newlines, and place line_next correctly
      line_next = i;
      if(in[i] != '\n' and in[i] != '\r') {
        //Double newline or newline at start
        lastchar_was_newline = false;
      }
      //If needed, break the loop here,
      // with line_next set corectly for the array shift
      if (not keepParsing) {
        break;
      }
    }
    else if (in[i] == '\n' or in[i] == '\r') {
      //Newline not following start or another newline
      memcpy(line_buff, in+line_next, i-line_next);
      line_buff[i-line_next]='\0';

      keepParsing = parse_line(line_buff);

      lastchar_was_newline = true;

      //Note: line_next is valid before lastchar_was_newline is false
      line_next = i;

      memset(line_buff,'\0', sizeof(line_buff));
      gotCommand = true;
    }
    //Else: Just keep counting chars
  }

  #ifdef PARSER_DEBUG
  if (buffCount > 0) {
    Serial.println();
    Serial.print  (F("@ buffCount    = "));
    Serial.println(buffCount);
    Serial.print  (F("@ line_next    = "));
    Serial.println(line_next);
    Serial.print  (F("@ lastchar_was_newline = "));
    Serial.println(lastchar_was_newline);
    Serial.print  (F("@ keepParsing         = "));
    Serial.println(keepParsing);
    for(size_t i = 0; i < ((buffCount+5) < buffSize ? (buffCount+5) : buffSize); i++) {
      Serial.print(F("@ "));
      Serial.print(i);
      Serial.print(i<buffCount ? " '" : " X '");
      Serial.print(in[i]);
      Serial.print(F("' = "));
      Serial.print((byte)in[i]);
      Serial.print(i==line_next ? F(" <-\n") : F("\n"));
    }
  }
  #endif

  //3. Prepare buffers for next round
  if (lastchar_was_newline && buffCount == line_next+1) {
    //All data was parsed;
    // reset buffer and continue

    #ifdef PARSER_DEBUG
    Serial.println(F("@ "));
    Serial.println(F("@ Buffer reset"));
    #endif

    if(not in[line_next+1] == '\0') {
      //Internal error!
      output_buff_flush();
      originatingStream->print(F("@ INFO BUFFER UNTERMINATED\n"));
      originatingStream->print(F("@ INFO BUFFER RESET\n"));
      //TODO:
      // This may indicate a serious bug (array overrun)
      // consider resetting the CPU!
    }

    //All treated, let's reset
    memset(line_buff,'\0', sizeof(line_buff));
    memset(in,'\0', buffSize);
    buffCount = 0;
    line_next = 0;

    // Ready for more
  }
  else if (line_next>0) {
    //Some data remains in buffer (whole or partial lines);
    // shift the remaining content of `in` to start of `in`

    #ifdef PARSER_DEBUG
    Serial.println(F("@ "));
    Serial.println(F("@ Shifting:"));
    #endif

    size_t j = 0;
    for(size_t i = line_next; i < buffCount; i++) {
      in[j] = in[i];
      j += 1;
    }

    memset(in+j,'\0', buffSize-j);
    buffCount = j;
    line_next = 0;

    #ifdef PARSER_DEBUG
    for(size_t i = 0; i < ((buffCount+5) < buffSize ? (buffCount+5) : buffSize); i++) {
      Serial.print(F("@ "));
      Serial.print(i);
      Serial.print(i<buffCount ? " '" : " X '");
      Serial.print(in[i]);
      Serial.print(F("' = "));
      Serial.print((byte)in[i]);
      Serial.print(i==line_next ? F(" <-\n") : F("\n"));
    }
    #endif
  }

  return keepParsing;
}
bool parse_line(char* line_buff) {
  //Parse one command line, given in "line_buff".
  // Return true to continue parsing lines from the input buffer,
  // or false to tell input_parser() to stop parsing lines now,
  // do the cleanup, and let loop() iterate to act on the command that was recieved.
  // This function assumes that line_buff is correctly NULL terminated.

  //General comment on "buff[x]": x should be the length of the longest possible string
  // for this buffer + 1 for the \0 terminator.


  bufferWrite(output_buff, sizeof(output_buff), output_buffCount, "> ");
  bufferWrite(output_buff, sizeof(output_buff), output_buffCount, line_buff);
  bufferWrite(output_buff, sizeof(output_buff), output_buffCount, '\n');

  size_t len = strlen(line_buff);

  //Note: The sub-options are not written out in strings, in order to save RAM.

  if (len >= 4 and strncmp(line_buff, "HELP", 4)==0) {
    char buff[7];

    bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< USAGE:\n"));

    bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("<  'HELP'                            : Get help\n"));

    bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("<  'TEMP'                            : Get temperature(s)\n"));

    bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("<  'GRABBER STATUS'                  : Get grabber status\n"));
    bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("<  'GRABBER POS <pos>'               : Goto the given position <pos> (integer)\n"));
    bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("<  'GRABBER CLOSE'                   : Close the grabber (goto POS = "));
    snprintf(buff, sizeof(buff), "%03d )\n", grabber_closed);
    bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff);
    bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("<  'GRABBER OPEN'                    : Open the grabber  (goto POS = "));
    snprintf(buff, sizeof(buff), "%03d )\n", grabber_open);
    bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff);

    bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("<  'FILTER <id> STATUS'              : Get camera filter status and config for filter <id> = "));
    for (int filter=0; filter<filter_numFilters-1; filter++){
      snprintf(buff, sizeof(buff), "%01d, ",filter+1);
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff);
    }
    snprintf(buff, sizeof(buff), "%01d\n",filter_numFilters);
    bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff);

    bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("<  'FILTER <id> POS <pos>'           : Goto the given position <pos> (integer)\n"));
    bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("<  'FILTER <id> IN'                  : Put filter <id> in   (FILTER <id> STATUS will show POS for this)\n"));
    bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("<  'FILTER <id> OUT'                 : Take filter <id> out (FILTER <id> STATUS will show POS for this)\n"));
    
    bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("<  'STEPPER STATUS'                  : Get the stepper status\n"));
    bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("<  'STEPPER POS <axis> <steps>'      : Goto the given position on the given axis\n"));
    bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("<  'STEPPER ZEROSEEK <axis>'         : Find the zero on the given axis (enables absolute mode)\n"));
    bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("<  'STEPPER RESET <axis>'            : Resets the given axis (sets position to 0 and disables absolute mode)\n"));
    bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("<  'STEPPER INTERLOCK ENABLE/DISABLE : Enable/disable the lockout of XY until Z is in absolute and above minimum\n"));

    bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< HELP DONE.\n"));
  }

  else if (len >= 4 and strncmp(line_buff, "TEMP", 4)==0) {
    char buff[23];

    snprintf(buff,sizeof(buff), "< TEMP #sensors: %03d\n", temp_numSensors);
    bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff);

    for (size_t i=0; i<temp_numSensors; i++) {
      float f = temp_data[i];
      int tmp_int = (int) (f*100);
      int tmp_dec = abs(tmp_int-tmp_int/100*100);
      tmp_int     = tmp_int/100;
      snprintf(buff, sizeof(buff), "< TEMP #%03d %+04d.%02d C\n", i, tmp_int,tmp_dec);
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff);
    }
  }

  else if (len >= 7 and strncmp(line_buff, "GRABBER", 7)==0) {
    if (len >= 14 and strncmp(line_buff+7, " STATUS", 7)==0)   { //'GRABBER STATUS'
      char buff[5];

      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< GRABBER CONFIG: CLOSE = "));
      snprintf(buff, sizeof(buff), "%03d", grabber_closed);
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff);

      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F(" OPEN = "));
      snprintf(buff, sizeof(buff), "%03d", grabber_open);
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff);

      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F(" LIMITS: MIN = "));
      snprintf(buff, sizeof(buff), "%03d", grabber_min);
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff);

      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F(" MAX = "));
      snprintf(buff, sizeof(buff), "%03d\n", grabber_max);
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff);

      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< GRABBER POS = "));
      snprintf(buff,sizeof(buff), "%03d\n", grabber_pos);
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff);

      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< EMERGENCY STOP = "));
      if(digitalRead(emergency_stop_pin) == emergency_isEmergency) {
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("TRUE\n"));
      }
      else {
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("FALSE\n"));
      }
    }
    else if (len >= 13 and strncmp(line_buff+7, " CLOSE", 6)==0)  { //'GRABBER CLOSE'
      if(grabber_go) {
        // This should not be possible!
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< ERROR GRABBER ALREADY SET"));
        return true;
      }

      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< GRABBER GOTO "));
      char buff[5];
      snprintf(buff,sizeof(buff), "%03d\n", grabber_closed);
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff);

      grabber_goto = grabber_closed;
      grabber_go   = true;
      return false;
    }
    else if (len >= 12 and strncmp(line_buff+7, " OPEN", 5)==0)  { //'GRABBER OPEN'
      if(grabber_go) {
        // This should not be possible!
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< ERROR GRABBER ALREADY SET"));
        return true;
      }

      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< GRABBER GOTO "));
      char buff[5];
      snprintf(buff,sizeof(buff), "%03d\n", grabber_open);
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff);

      grabber_goto = grabber_open;
      grabber_go   = true;
      return false;
    }
    else if (len >= 11 and strncmp(line_buff+7, " POS", 4) == 0) { //'GRABBER POS'
      int scanStatus = 0;
      if (len < 13) {
        //no new POS integer given?
        scanStatus = 0;
      }
      else if (len > 15) {
        //More than three digits given or extra space and >2 digits?
        scanStatus = 0;
      }
      else if (line_buff[11] != ' ') {
        //No separating space?
        scanStatus = 0;
      }
      else {
        //Passed sanity checks -> scan!
        scanStatus = sscanf(line_buff+12," %d",&grabber_goto);
      }

      if (scanStatus == 1){
        //Got one thing from sscanf()!
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< GRABBER GOTO "));
        char buff[5];
        snprintf(buff,sizeof(buff), "%03d\n", grabber_goto);
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff);
        if (grabber_goto >= grabber_min && grabber_goto <= grabber_max) {
          grabber_go=true;
          return false;
        }
        else {
          bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< ERROR GRABBER POS OUTSIDE LEGAL RANGE\n"));
          grabber_goto = grabber_pos;
        }
      }
      else {
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< ERROR GRABBER POS PARSE FAILURE\n"));
      }

    }
    else {
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< ERROR GRABBER COMMAND NOT RECOGNIZED\n"));
    }

  }

  else if (len >= 7 and strncmp(line_buff, "FILTER ", 7)==0) {
    //Which filter?
    int selected_filter=-1;
    for (int filter=0; filter<filter_numFilters; filter++){
      char filterID[2];
      snprintf(filterID, sizeof(filterID), "%01d", filter+1);
      if (len >= 8 and strncmp(line_buff+7, filterID,1)==0){
        selected_filter=filter;
      }
    }
    if (selected_filter==-1) {
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< ERROR FILTER ID ='"));
      char buff[5];
      //strncpy(buff,line_buff+7,1);
      buff[0]=*(line_buff+7);
      buff[1]='\0';
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff);
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("' PARSE FAILURE\n"));

      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< EXPECTED INTEGER BETWEEN 1 AND "));
      snprintf(buff, sizeof(buff), "%01d", filter_numFilters);
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff);
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("\n"));

      return true;
    }

    if (len >= 15 and strncmp(line_buff+8, " STATUS", 7)==0)   { //FILTER X STATUS
      char buff[5];

      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< FILTER "));
      snprintf(buff, sizeof(buff), "%01d", selected_filter+1);
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff);
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F(" CONFIG: IN = "));
      snprintf(buff, sizeof(buff), "%03d", filter_in[selected_filter]);
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff);

      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F(" OUT = "));
      snprintf(buff, sizeof(buff), "%03d", filter_out[selected_filter]);
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff);

      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F(" LIMITS: MIN = "));
      snprintf(buff, sizeof(buff), "%03d", filter_min);
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff);

      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F(" MAX = "));
      snprintf(buff, sizeof(buff), "%03d\n", filter_max);
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff);

      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< FILTER "));
      snprintf(buff, sizeof(buff), "%01d", selected_filter+1);
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff);
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F(" POS = "));
      snprintf(buff,sizeof(buff), "%03d\n", filter_pos[selected_filter]);
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff);

      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< EMERGENCY STOP = "));
      if(digitalRead(emergency_stop_pin) == emergency_isEmergency) {
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("TRUE\n"));
      }
      else {
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("FALSE\n"));
      }
    }
    else if (len >= 11 and strncmp(line_buff+8, " IN", 3)==0)  { //'FILTER X IN'
      if(filter_go[selected_filter]) {
        // This should not be possible!
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< ERROR FILTER "));
        char buff[5];
        snprintf(buff, sizeof(buff), "%01d", selected_filter+1);
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff);
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F(" ALREADY SET\n"));
        return true;
      }

      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< FILTER "));
      char buff1[5];
      snprintf(buff1, sizeof(buff1), "%01d", selected_filter+1);
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff1);
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F(" GOTO "));
      char buff2[5];
      snprintf(buff2,sizeof(buff2), "%03d\n", filter_in[selected_filter]);
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff2);

      filter_goto[selected_filter] = filter_in[selected_filter];
      filter_go[selected_filter]   = true;
      return false;
    }//IN
    else if (len >= 12 and strncmp(line_buff+8, " OUT", 4)==0)  { //'FILTER X OUT'
      if(filter_go[selected_filter]) {
        // This should not be possible!
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< ERROR FILTER "));
        char buff[5];
        snprintf(buff, sizeof(buff), "%01d", selected_filter+1);
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff);
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F(" ALREADY SET\n"));            
        return true;
      }

      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< FILTER "));
      char buff1[5];
      snprintf(buff1, sizeof(buff1), "%01d", selected_filter+1);
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff1);
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F(" GOTO "));

      char buff2[5];
      snprintf(buff2,sizeof(buff2), "%03d\n", filter_out[selected_filter]);
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff2);

      filter_goto[selected_filter] = filter_out[selected_filter];
      filter_go[selected_filter]   = true;
      return false;
    }//OUT
    else if (len >= 12 and strncmp(line_buff+8, " POS", 4) == 0) { //'FILTER X POS'
      int scanStatus = 0;
      if (len < 14) {
        //no new POS integer given?
        scanStatus = 0;
      }
      else if (len > 16) {
        //More than three digits given or extra space and >2 digits?
        scanStatus = 0;
      }
      else if (line_buff[12] != ' ') {
        //No separating space?
        scanStatus = 0;
      }
      else {
        //Passed sanity checks -> scan!
        scanStatus = sscanf(line_buff+13," %d",&filter_goto[selected_filter]);
      }

      if (scanStatus == 1){
        //Got one thing from sscanf()!
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< FILTER "));
        char buff1[5];
        snprintf(buff1, sizeof(buff1), "%01d", selected_filter+1);
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff1);
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F(" GOTO "));

        char buff2[5];
        snprintf(buff2,sizeof(buff2), "%03d\n", filter_goto[selected_filter]);
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff2);
        if (filter_goto[selected_filter] >= filter_min && filter_goto[selected_filter] <= filter_max) {
          filter_go[selected_filter]=true;
          return false;
        }
        else {
          bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< ERROR FILTER "));
          char buff[5];
          snprintf(buff, sizeof(buff), "%01d", selected_filter+1);
          bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff);
          bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F(" POS OUTSIDE LEGAL RANGE\n"));

          filter_goto[selected_filter] = filter_pos[selected_filter];
        }
      }
      else {
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< ERROR FILTER "));
        char buff[5];
        snprintf(buff, sizeof(buff), "%01d", selected_filter+1);
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff);
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F(" POS PARSE FAILURE\n"));

      }

    }//POS ends
    else {
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< ERROR FILTER COMMAND '"));
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, line_buff+8);
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("' NOT RECOGNIZED\n"));
    }
  }//FILTER ends


  else if (len >= 7 and strncmp(line_buff, "STEPPER", 7)==0) {
    if (len >= 14 and strncmp(line_buff+7, " STATUS", 7)==0)   { //'STEPPER STATUS'
      if (stepper_interlock) {
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< STEPPER INTERLOCK ENABLED (SAFE)\n"));
      }
      else {
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< STEPPER INTERLOCK DISABLED (UNSAFE)\n"));
      }

      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< EMERGENCY STOP = "));
      if(digitalRead(emergency_stop_pin) == emergency_isEmergency) {
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("TRUE\n"));
      }
      else {
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("FALSE\n"));
      }

      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< STEPPER AX         POS ABS MIN MAX\n"));
      //Print "< STEPPER <X/Y/Z> <POS> <ABS> <MIN> <MAX>"
      for (size_t i=0; i<stepper_numAxis; i++) {

        bool onMAX = (digitalRead(stepper_switchMAX_pin[i]) == stepper_switch_active);
        bool onMIN = (digitalRead(stepper_switchMIN_pin[i]) == stepper_switch_active);
        char buff[38];
        snprintf(buff, sizeof(buff), "< STEPPER  %c %+011ld   %1d   %1d   %1d\n",
                 stepper_axnames[i], stepper_pos[i],
                 stepper_mode_absolute[i], onMIN, onMAX);
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff);
      }

    }// STATUS
    else if (len >= 11 and strncmp(line_buff+7, " POS", 4)==0) { // 'STEPPER POS <axis> <steps>'
      stepper_go_axis = parse_line_stepperAxis(line_buff, 11, len);
      if (stepper_go_axis == SIZE_MAX) {
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< ERROR STEPPER POS AXIS PARSE FAILURE\n"));
        stepper_go_axis = 0;
        return true;
      }

      //Find wanted position
      int scanStatus = 0;
      if (len < 15) {
        //no new POS integer given?
        scanStatus = 0;
      }
      else if (len > 22) {
        //More than seven digits given (including sign)?
        scanStatus = 0;
      }
      else if (line_buff[13] != ' ') {
        //No separating space?
        scanStatus = 0;
      }
      else {
        //Passed sanity checks -> scan!
        scanStatus = sscanf(line_buff+14," %ld",&stepper_goto);
      }

      if (scanStatus == 1){
        //Got one thing from sscanf()!
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< STEPPER GOTO "));
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, stepper_axnames[stepper_go_axis]);
        char buff[14];
        snprintf(buff,sizeof(buff), " %+011ld\n", stepper_goto);
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff);
        stepper_go = true;
        return false;
      }
      else {
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< ERROR STEPPER POS STEPS PARSE FAILURE\n"));
        stepper_go_axis = 0;
        stepper_go = false;
        return true;
      }

    }//POS

    else if (len >= 16 and strncmp(line_buff+7, " ZEROSEEK", 9)==0) { // 'STEPPER ZEROSEEK <axis>'
      stepper_go_axis = parse_line_stepperAxis(line_buff, 16, len);
      if (stepper_go_axis == SIZE_MAX) {
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< ERROR STEPPER ZEROSEEK AXIS PARSE FAILURE\n"));
        stepper_go_axis = 0;
        return true;
      }
      stepper_go_zero = true;
      stepper_go = true;
      return false;

    }

    else if (len >= 13 and strncmp(line_buff+7, " RESET", 6)==0) { // 'STEPPER RESET <axis>'
      size_t tmpAx = 0;
      tmpAx = parse_line_stepperAxis(line_buff, 13, len);
      if (tmpAx == SIZE_MAX) {
        bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< ERROR STEPPER RESET AXIS PARSE FAILURE\n"));
        tmpAx = 0;
        return true;
      }

      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< STEPPER RESET AXIS "));
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, stepper_axnames[tmpAx]);
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, '\n');

      stepper_pos[tmpAx] = 0L;
      stepper_mode_absolute[tmpAx] = false;
    }

    else if (len >= 24 and strncmp(line_buff+7, " INTERLOCK ENABLE", 17) == 0) { // 'STEPPER INTERLOCK ENABLE'
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< STEPPER INTERLOCK NOW ENABLED (SAFE)\n"));
      stepper_interlock = true;
    }

    else if (len >= 25 and strncmp(line_buff+7, " INTERLOCK DISABLE", 18) == 0) { // 'STEPPER INTERLOCK DISABLE'
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< STEPPER INTERLOCK NOW DISABLED (UNSAFE)\n"));
      stepper_interlock = false;
    }

    else {
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< ERROR STEPPER COMMAND NOT RECOGNIZED\n"));
    }
  }

  else {
    bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< ERROR COMMAND NOT RECOGNIZED, SAY 'HELP' TO GET STARTED\n"));
  }

  //Default: Don't halt the parsing
  return true;
}

size_t parse_line_stepperAxis(char* line_buff, size_t offset, size_t len) {
  //Helper for parsing the STEPPER commands
  // Offset is where to start to look (i.e. the space before axname)
  // Returns the corresponding index in the stepper arrays,
  // or SIZE_MAX if parsing failed.

  //Currently requires exactly 1 space before the axname

  //Find axis; expected format: ' <axis>', e.g. ' X'.
  char axisChar = '\0';
  if (len < (offset+2)) {
    //Too short
    axisChar = '\0';
  }
  else if (line_buff[offset] != ' ') {
    //Strange format
    axisChar = '\0';
  }
  else {
    axisChar = line_buff[offset+1];
  }

  size_t ax_i = 0;
  bool ax_ok = false;
  for (size_t i=0; i<stepper_numAxis; i++) {
    if (axisChar == stepper_axnames[i]) {
      ax_i = i;
      ax_ok = true;
      break;
    }
  }

  if (not ax_ok) {
    //Will also be triggered if axisChar='\0',
    // as search will fail
    ax_i = SIZE_MAX;
  }
  return ax_i;
}



int bufferWrite(char* buff, const size_t buffSize, size_t& buffCount, const char* in) {
  /* Add a zero-terminated string to a buffer,
   * checking for buffer overruns and
   * (SIDE EFFECT!) incrementing the buffCount.
   * If the buffer will overrun, truncate the input string.
   * Return number of chars written, or return <0 on error.
   * If buffer overrun, chars may still be written -> positive return.
   */

  size_t len = strlen(in);

  int ret    = snprintf(buff+buffCount, buffSize-buffCount, in);
  buffCount += ret;
  return ret;
}
int bufferWrite(char* buff, const size_t buffSize, size_t& buffCount, const char in) {
  /* Add a single char to a buffer,
   * checking that there is space for it and
   * (SIDE EFFECT!) incrementing the buffCount.
   * Returns the number of chars written (0 or 1).
   */
  if (buffCount == buffSize-1) {
    //Pointing at terminating \0,
    // no more space!
    return 0;
  }
  else {
    buff[buffCount] = in;
    buffCount++;
    return 1;
  }
}
int bufferWrite(char* buff, const size_t buffSize, size_t& buffCount, const __FlashStringHelper *ifsh) {
  //Copy flash contents into buffer, otherwise functionally identical to the const char* version.
  // Used to save RAM from big hardcoded strings.
  //
  // Based on ArduinoCore-avr/cores/arduino/Print.cpp->Print::Print(const __FlashStringHelper *ifsh)
  // If it doesn't work (future arch), try removing the 'F()' around the strings and comment out this function.

  PGM_P p = reinterpret_cast<PGM_P>(ifsh);
  size_t n = 0;
  while (1) {
    char c = pgm_read_byte(p++);
    if (c == 0) break;
    if (bufferWrite(buff, buffSize, buffCount, c)) n++;
    else break;
  }
  return n;
}





#ifdef USE_DHCP
void DHCPhousekeeping() {
  switch (Ethernet.maintain()) {
    //output_buff_flush();
    case 1:
      //renewed fail
      Serial.print(F("@ DHCP Error: renewed fail\n"));
      break;

    case 2:
      //renewed success
      Serial.print(F("@ DHCP Renewed success\n"));
      //print your local IP address:
      Serial.print(F("@ DHCP My IP address: "));
      Serial.print(Ethernet.localIP());
      Serial.print('\n');
      break;

    case 3:
      //rebind fail
      Serial.print(F("@ DHCP Error: rebind fail\n"));
      break;

    case 4:
      //rebind success
      Serial.print(F("@ DHCP Rebind success\n"));
      //print your local IP address:
      Serial.print(F("@ DHCP My IP address: "));
      Serial.print(Ethernet.localIP());
      Serial.print('\n');
      break;

    default:
      //nothing happened
      break;
  }
}
#endif
