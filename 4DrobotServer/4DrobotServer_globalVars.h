#ifndef __4DrobotServer_globalVars_h__
#define __4DrobotServer_globalVars_h__

/*
 * This file is part of 4DrobotServer.
 * 4DrobotServer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 4DrobotServer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with 4DrobotServer. If not, see <https://www.gnu.org/licenses/>.
 */

//Note: This isn't a proper header file, as it contains implementations (variable = value).
//      It can therefore only be included in one compilation unit.
//      This is OK as long as we stay within Arduino studio and only use .ino files for code,
//      which are concatonated in alphabetical order.

// Sockets for TCP communications
EthernetServer telnet_socket(telnet_port);
EthernetClient telnet_connection[telnet_numConnections];

// ASCII TTY IO Buffers
// buffCount is the number of elements = idx of first free element
// buffCount_prev is buffCount in the start of the previous parsing round.
//  It is used to print a "$" when done; if now == 0 and prev !=0
//  then this cycle cleared the data from the previous round and are ready.
char           telnet_buff[telnet_numConnections][telnet_bufflen];
size_t         telnet_buffCount[telnet_numConnections];
size_t         telnet_buffCount_prev[telnet_numConnections];

char           serial_buff[telnet_bufflen];
size_t         serial_buffCount;
size_t         serial_buffCount_prev;

char           output_buff[output_bufflen];
size_t         output_buffCount;

// Temperature sensor access & buffers

OneWire            temp_oneWire[temp_numSensorPins];
DallasTemperature  temp_sensors[temp_numSensorPins];
float              temp_data[temp_numSensors]; //[degC]
unsigned long      temp_update_prev = 0;       //[ms]

// Sample grabber servo
#ifdef USE_STDSERVOLIB
Servo grabber_servo;
#else
Adafruit_TiCoServo grabber_servo;
#endif
bool  grabber_go   = false;
int   grabber_goto = 0;
int   grabber_pos  = grabber_closed; //Assumed initial position

// Camera filter servo
#ifdef USE_STDSERVOLIB
Servo filter_servo[filter_numFilters];
#else
Adafruit_TiCoServo filter_servo[filter_numFilters];
#endif


 //{FILTER1, FILTER2}
bool  filter_go[]   = {false, false};
int   filter_goto[] = {0,180};
int   filter_pos[]  = {filter_out[0],filter_out[1]}; //Assumed initial position




// Stepper motors

bool     stepper_go              = false;               //Move a stepper on the next loop?
bool     stepper_go_zero         = false;               //Move-to-position   (false) or
                                                        // move-to-switchLOW (true)?
size_t   stepper_go_axis         = 0;                   //Which stepper to move?
long int stepper_goto            = 0;                   //[steps] Target position
                                                        // (used if stepper_go_zero==false)
bool     stepper_interlock       = true;                //Is the stepper interlock enabled (safe)
                                                        //                   or disabled (unsafe)?
long int stepper_pos[]           = {0,0,0};             //Current stepper position
bool     stepper_mode_absolute[] = {false,false,false}; //Do we have a valid zeroing?
                                                        // If true, pos represents the absolute
                                                        // position of that axis

const unsigned int stepper_maxwait_us = 16383;                       // [us] Maximum value allowed by delayMicroseconds
unsigned int       stepper_accel_profile[stepper_accelerate_steps];  // [us] Stepper acceleration profile
size_t             stepper_accelerate_steps_slow;                    // Truncate the accel_profile after this many steps
                                                                     //      when moving slowly
#endif
