#ifndef __4DrobotServer_config_h__
#define __4DrobotServer_config_h__

/*
 * This file is part of 4DrobotServer.
 * 4DrobotServer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 4DrobotServer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with 4DrobotServer. If not, see <https://www.gnu.org/licenses/>.
 */

//Note: This isn't a proper header file, as it contains implementations (variable = value).
//      It can therefore only be included in one compilation unit.
//      This is OK as long as we stay within Arduino studio and only use .ino files for code,
//      which are concatonated in alphabetical order.

//Note on Arduino pins used for Ethernet shield,
// AVOID THESE PINS:
// 4/10/11/12/13 (UNO)
// 50/51/52/10/4; 53 unused but it MUST be an output (Mega)

// ***** NETWORK CONFIG ************************

// MAC address for the Arduino on the plasma lens
//byte mac[] = {0xA8, 0x61, 0x0A, 0xAE, 0x84, 0xED};

// MAC address for the Arduino on the CLIC structure
//byte mac[] = {0xA8, 0x61, 0x0A, 0xAE, 0x84, 0xEE};

// MAC address for Kyrre's test board
//byte mac[] = {0xA8, 0x61, 0x0A, 0xAE, 0x72, 0xF7};

// MAC address for the Arduino on the 4DRobot (Office Network)
//byte mac[] = {0xA8, 0x61, 0x0A, 0xAE, 0x84, 0xEF};

// MAC address for the Arduino on the 4DRobot (Technical Network, CORRECT one)
byte mac[] = {0xA8, 0x61, 0x0A, 0xAE, 0x71, 0xAC};

//MAC address for Vilde's test board
//byte mac[] = {0xA8, 0x61, 0x0A, 0xAE, 0xDE, 0x0A};

//To use DHCP or not to use DHCP, that's the question.
// Comment out to use static IP configuration
#define USE_DHCP

#ifndef USE_DHCP
IPAddress ip(172, 26, 91, 239);
//IPAddress ip(192, 168, 1, 2);
IPAddress myDns(137, 138, 16, 5);
IPAddress gateway(172, 26, 91, 1);
//IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);
#endif

const int    telnet_port = 23;

//Number of connection 'slots'
const size_t telnet_numConnections = MAX_SOCK_NUM;

//This quickly takes a LOT of memory!
//The total for MEGA is 8k, and we also need some stack space!
//Note: It's possible to reduce the number of connection slots.
const size_t telnet_bufflen = 256;
const size_t output_bufflen = 2048;

// ***** LOCAL CONTROL CONFIG ******************
const int emergency_stop_pin = 9;
const bool emergency_isEmergency = HIGH;

// ***** TEMPERATURE SENSOR CONFIG *************

const size_t  temp_numSensorPins         = 2;
const uint8_t temp_oneWire_pins[]        = {48,49};
const size_t  temp_numSensors_perpin[]   = {1,1};
const size_t  temp_numSensors            = 2;
const unsigned long temp_update_interval = 1000; //[ms]

const uint8_t temp_numRetries = 5;

// ***** SAMPLE GRABBER SERVO CONFIG ***********

// TODO: Tune stepWait, positions
// Note: Adafruit_TiCoServo requires pin 9/10 (Uno) or 2/3/5/6/7/8/11/12/13/44/45/46 (Mega) as it needs hardware PWM capability.
const int grabber_servo_pin    =   5;
const int grabber_closed       =  85; //[deg]
const int grabber_open         = 130; //[deg]
const int grabber_min          =   0; //[deg]
const int grabber_max          = 180; //[deg]
const int grabber_stepWait     =  50; //[ms]

// ***** CAMERA FILTER SERVO CONFIG ************
// Yeah, a struct would be cleaner, then we could shared the code with grabber...
// {Filter1, filter2}

const size_t filter_numFilters = 2; //Max 9 filters, since we are assuming single digit filter ID many places
const int filter_servo_pin[] =   {8,13}; 
const int filter_in[]        =   {5,165}; //[deg]
const int filter_out[]       = {165,5}; //[deg]
const int filter_min       =   0; //[deg]
const int filter_max       = 180; //[deg]
const int filter_stepWait  =  10; //[ms]
const uint16_t filter_minpulsewidth  =  544; //[us], default=544
const uint16_t filter_maxpulsewidth  =  2400; //[us], default=2400

// ***** STEPPER CONFIGS ***********************

const size_t stepper_numAxis                 = 3;

//Array indices of the 3 axis
#define STEPPER_X 1
#define STEPPER_Y 0
#define STEPPER_Z 2

const char    stepper_axnames[]              = {'Y','X','Z'};

const uint8_t stepper_dir_pin[]              = {2,6,11}; //Stepper direction pins (HIGH=positive, LOW=negative)
const uint8_t stepper_step_pin[]             = {3,7,12}; //Stepper pulse-to-step pins (pulse HIGH, otherwise low)

//Define level for stepper_dir_pin[] to count steps in forward direction
// Used to define the Z axis as zero when on top, so that we can zero it first on top switch,
// then leave it on the limit switch while zeroing the others (safe).
const bool    stepper_forward[]              = {LOW,HIGH,HIGH};
const bool    stepper_backward[]             = {HIGH,LOW,LOW};

const uint8_t stepper_switchMAX_pin[]         = {33,27,43}; //positive steps limit switch pins
const uint8_t stepper_switchMIN_pin[]         = {32,26,42}; //negative steps limit switch pins
//TODO: Verify active/inactive -> HIGH/LOW
//      Note: Needs pulldown/pullup resistori
//      Note: Probably safest to define on-limit = open, so if a switch fails the axis is safe (but stopped)
const bool    stepper_switch_active          = LOW;    //On limit switch when pin active
const bool    stepper_switch_inactive        = HIGH;   //On limit switch when pin inactive

//If tuning timings, make sure to enable ACCELERATION_DEBUG and check that nothing overflows etc.
// Note that code execution time of the normal move is about 29us, and 19us for zeroseek (measured with a scope).
// This is effectively added to the stepper_delay.
const unsigned int  stepper_onTime           = 25;    // [us] How long to hold the step pin HIGH
const unsigned int  stepper_delay_fast       = 1000;  // [us] How long to wait between steps at flat-top when moving fast
                                                      //      Note: stepper_onTime will be subtracted from this
                                                      //            when setting the waiting from falling to rising edge.
                                                      //            Computation time is not taken into account.
const unsigned int  stepper_delay_slow       = 10000; // [us] How long to wait between steps at flat-top when moving slow
                                                      //      Note: It will just truncate the normal accel profile
                                                      //            to the longest delay above this value.
                                                      //            If this value is longer than the longest in the accel profile,
                                                      //            there will be no acceleration, it will start directly at this speed.
const size_t        stepper_accelerate_steps = 300;   // When accelerating, how many steps to use for ramp to fast speed

// When at high speed, ramp down to slow speed when
// pos < accelerate_steps+slowzone or pos > (rail_length-accelerate_steps-slowzone)
// and when pos approaches the target position
//TODO: implement
const unsigned long int  stepper_rail_length[]    = {7229,7798,2802}; // [steps] Total length of the rails
const unsigned long int  stepper_slowzone_steps   = 50;               // [steps] Take it easy when approaching limit switches

const unsigned long int stepper_maxsteps_move[]   = {7500,8100,3100}; // [steps] Maximum allowed displacement from current zero position (abs mode or not).
                                                                      // This should limit the damage in case of limit switch failure.
                                                                      // TODO: In abs mode, we probaby should not allow commands to go to negatice position...

//Interlocks
const long int           stepper_Zpos_unlockXY         = 1400;   // [steps] Only allow XY movements when
                                                                 //          Zpos < Zpos_unlockXY and Z axis is in absolute mode
const unsigned long int  stepper_onlimit_steps_towards = 2;      // [steps] Number of steps to allow when a limit switch is active
                                                                 //         (moving towards the switch, i.e. allow
                                                                 //          this many hits before stopping in case of spurious hits)
                                                                 //         Note: For ZEROSEEK, this limit is always 0,
                                                                 //               however there is a restart/retry if it is not stable.
const unsigned long int  stepper_onlimit_steps_away    = 100;    // [steps] Number of steps to allow when a limit switch is active
                                                                 //         (moving away from the switch, i.e. allow
                                                                 //          this many hits before stopping in order to
                                                                 //          allow escape from switch)

//When zeroseeking, confirm the limit switch a few times before calling it good and going in absolute mode.
const int stepper_zeroseek_confirmHitCounts = 5;
const unsigned int stepper_zeroseek_confirmHitDelay = 1000; //[us]

// ***** INTERNAL CONFIGS FOR DEV ***************

// Uncomment to include extra debug messages about parser (sent to Serial only)
//#define PARSER_DEBUG

// Uncomment to include extra debug messages about sensors (sent to Serial only)
//#define SENSOR_DEBUG

// Uncomment to include extra debug messages about acceleration (sent to Serial only)
//#define ACCELERATION_DEBUG

// Uncomment to use standard Arduino Servo library instead of TiCoServo from Adafruit.
// This will cause the grabber to twich, but will work on any pin and any Arduino
//#define USE_STDSERVOLIB

// ***** HARDWARE SIMULATION (FOR DEV) *********

//Uncomment to simulate hardware
//#define DUMMY_TEMP
//#define DUMMY_SERVO
//#define DUMMY_STEPPER
//#define NO_ETHERNET


#endif
