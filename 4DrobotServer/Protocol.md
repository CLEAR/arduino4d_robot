# C-robot network protocol

To connect:
use NetCat (nc) on port 23:
 nc 192.168.1.31 23
Telnet can work too, however it often tries to negotiate things.


## General rules about input and output:

 * Input is treated line by line
 * Once booted, TCP clients and Serial is treated identically.
 * Input from everyone is buffered separately.
   When buffer contains a one or more newlines (`\n` or `\r` or a combination of these),
   these lines are processed.
 * If an input buffer becomes full, the content's of that buffer is discarded,
   as there isn't space for a newline to trigger parsing and draining.
   The content of the buffer is NOT inspected, so valid command lines
   in the beginning of the buffer may be ignored!
   Note: The buffer is protected from overrun.
 * Input lines are echoed back to everyone, with a `> ` prepended.
 * Results are sent to everyone, with a `< ` prepended.
 * Client-specific errors and warnings are sent to a single client/serial,
   with a `@ ` prepended.
 * When ready for input from a client/Serial, a `$` is sent.
 * Everything sent is terminated by a `\n`, including `$`.

## Automatic status messages (tree):
 * `INFO` : Information about robot status
   * `BOOT` (`@` prefix only, only on serial): General information during booting
   * `BUFFER` (`@` prefix only, only on serial): Something about *this* input buffer
     * `NULL`: The buffer of this connection contained a `\0` before the end.
     * `FULL`: The buffer of this connection was full.
     * `UNTERMINATED`: The buffer of this connection did not have a `\0` at the end.
                       This is an internal error and should never happen.
     * `RESET`: The buffer was reset, throwing away anything in it that hasn't been acknowledged.
   * `CONN`: Something to do with the connection of a client
     * `MAKE: <IP> ON #<SLOT>`: A new client connected
     * `BREAK:  <IP> ON #<SLOT>`: A client disconnected

## Input commands (tree) and responses:
 * `HELP`: Print possible commands
 * `GET`: Request information about the status of something.
   * `TEMP`: Get the temperature sensor data.
     Output (including `< `), one line pr. sensor:
     * `< TEMP sensorID TEMP C`