/*
 * Server for controling 4D robot for moving samples in/out of the beam at CLEAR.
 * Access via telnet-like interface at port 23.
 * 
 * Example for accessing over text interface:
 *  nc 192.168.1.31 23
 * Netcat (nc) is the best client: it is a clean TCP client,
 * which makes no attempts to negotiate anything.
 * 
 * Kyrre Ness Sjobak, 2021-
 * University of Oslo / CERN
 * 
 * Inspired by PositionGaugeServer, also in use at CLEAR.
 */

/* 
 * This file is part of 4DrobotServer.
 * 4DrobotServer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 4DrobotServer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with 4DrobotServer. If not, see <https://www.gnu.org/licenses/>. 
 */

#include <Arduino.h>

#include <SPI.h>
#include <Ethernet.h>

#include <OneWire.h>
#include <DallasTemperature.h>

//This file holds all the configurations
// to be modified for deployment
#include "4DrobotServer_config.h"

#ifdef USE_STDSERVOLIB
#include <Servo.h>
#else
#include <Adafruit_TiCoServo.h>
#endif

//This file holds all the global variables
// it does not need to be modified
#include "4DrobotServer_globalVars.h"

void setup() {
  //Serial setup, for debugging
  Serial.begin(9600);
  
  // wait for serial port to connect. Needed for native USB port only
  int serialCounter=0;
  while (!Serial) {
    //However only wait for 1 second before giving up,
    // so that it can work also when not connected to USB
    delay(100);
    serialCounter++;
    if (serialCounter > 10) break;
  }
  Serial.print('\n');
  Serial.print("@ INFO BOOT\n");
  Serial.print("@ INFO BOOT\n");
  Serial.println(F("@ INFO BOOT 4DrobotServer initializing..."));

  pinMode(emergency_stop_pin, INPUT_PULLUP);

  setup_networkIO();

  setup_servo();
  setup_stepper();

  setup_temperatures();

  //Serial is ready for input!
  Serial.print(F("@ INFO MSG Say 'HELP' to get started\n"));
}

//Run all the "programs" in order
void loop() {
  // Run actuators (one at a time)
  servo_control();
  stepper_control();

  // Update sensor readings
  temperatures_update();

  // Communicate
  telnet_server();

  // Housekeeping
  #ifdef USE_DHCP
  DHCPhousekeeping();
  #endif
}
