/* 
 * This file is part of 4DrobotServer.
 * 4DrobotServer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 4DrobotServer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with 4DrobotServer. If not, see <https://www.gnu.org/licenses/>. 
 */

void setup_temperatures() {
  //Initialize temperature sensors to something nonsensical
  for (size_t i=0; i<temp_numSensors; i++) {
    temp_data[i] = -500.0f; //(it's float not Farenheit)
  }

  //Initialize buses
  for(size_t i=0; i<temp_numSensorPins; i++) {
    temp_oneWire[i] = OneWire(temp_oneWire_pins[i]);
    temp_sensors[i] = DallasTemperature(&(temp_oneWire[i]));
  }
}

//Program for refreshing the temperatures
void temperatures_update() {
  //Poll temperature sensors no faster than the given interval
  //If too short, some sensors may heat up and give inaccurate results
  unsigned long thisUpdateTime = millis();
  if (not (thisUpdateTime - temp_update_prev >= temp_update_interval)) {
    //Not yet
    return;
  }
  temp_update_prev = thisUpdateTime;

  //Refresh temp_data
  #ifndef DUMMY_TEMP
  size_t k = 0;
  for(size_t i=0; i<temp_numSensorPins; i++) {

    temp_sensors[i].requestTemperatures();
    for (size_t j=0; j<temp_numSensors_perpin[i]; j++) {
      size_t l = 0;
      for (l=0; l < temp_numRetries; l++) {
        temp_data[k] = temp_sensors[i].getTempCByIndex(j);
        if (temp_data[k] != -127) {
          break;
        }
      }

      #ifdef SENSOR_DEBUG
      if (l > 0) {
        Serial.print(F("@ TEMP: Neded "));
        Serial.print(l);
        Serial.println(F(" retries to get temperature"));
      }
      #endif

      k++;
    }

  }
  #else
  for (size_t i=0; i < temp_numSensors; i++) {
    temp_data[i] = random(-1000L,15000L)/100.0f;
  }
  #endif
}
