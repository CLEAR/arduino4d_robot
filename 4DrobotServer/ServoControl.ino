/*
 * This file is part of 4DrobotServer.
 * 4DrobotServer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 4DrobotServer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with 4DrobotServer. If not, see <https://www.gnu.org/licenses/>.
 */

void setup_servo() {
  //Sanity check grabber config
  if (not (grabber_open >= grabber_min && grabber_open <= grabber_max)) {
    Serial.print(F("@ Grabber config error; grabber_open outside of legal range\n"));
    while(true) {
      delay(1);
    }
  }
  if (not (grabber_closed >= grabber_min && grabber_closed <= grabber_max)) {
    Serial.print(F("@ Grabber config error; grabber_closed outside of legal range\n"));
    while(true) {
      delay(1);
    }
  }

  //Sanity check filter config
  for (int i = 0; i < filter_numFilters; i++){
      if (not ((filter_in[i] >= filter_min && filter_in[i] <= filter_max))) {
        Serial.print(F("@ Filter config error; filter_in outside of legal range\n"));
        while(true) {
          delay(1);
        }
      }
      if (not ((filter_out[i]  >= filter_min && filter_out[i] <= filter_max))) {
        Serial.print(F("@ Filter config error; filter_out outside of legal range\n"));
        while(true) {
          delay(1);
        }
      }
    }

  //Initialize Servo library
  #ifndef DUMMY_SERVO
  grabber_servo.attach(grabber_servo_pin);
  grabber_servo.write(grabber_pos);

  for (int i = 0; i < filter_numFilters; i++){
      filter_servo[i].attach(filter_servo_pin[i], filter_minpulsewidth, filter_maxpulsewidth);
      filter_servo[i].write(filter_pos[i]);      
  }
  #endif
}

//Program for controlling the servomotors for the grabber and camera filters
void servo_control() {  
  bool filterGo = false;
  for (int i = 0; i < filter_numFilters; i++){
    //int i =1;
    if (filter_go[i]) {
      filterGo = true;
      break;
      
    } 
  }
  if (not (grabber_go || filterGo)) {
      return;
  }

  //Move!

  if (grabber_go) {
    grabber_control();
  }

  for (int i = 0; i < filter_numFilters; i++){
    if (filter_go[i]) {
      filter_control(i);
    }    
  }  
}

void grabber_control() {
  bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< GRABBER MOVING...\n"));
  output_buff_flush();

  while (grabber_pos != grabber_goto) {
    if (digitalRead(emergency_stop_pin) == emergency_isEmergency) {
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< GRABBER ERROR EMERGENCY STOP\n"));
      break;
    }
    
    if (grabber_pos < grabber_goto) {
      grabber_pos++;
    }
    else {
      grabber_pos--;
    }

    #ifndef DUMMY_SERVO
    grabber_servo.write(grabber_pos);
    #else
    Serial.print(F("@ Grabber to: "));
    Serial.print(grabber_pos);
    Serial.print('\n');
    #endif

    delay(grabber_stepWait);
  }

  grabber_go   = false;
  stepper_goto = 0;

  bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< GRABBER GO FINISHED POS = "));
  char buff[5];
  snprintf(buff,sizeof(buff), "%03d\n", grabber_pos);
  bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff);

  output_buff_flush();
}

void filter_control(int filter) {
  char filterN[5];
  snprintf(filterN, sizeof(filterN), "%01d", filter+1);
  
  bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< FILTER "));
  bufferWrite(output_buff, sizeof(output_buff), output_buffCount, filterN);
  bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F(" MOVING...\n"));
  output_buff_flush();

  while (filter_pos[filter] != filter_goto[filter]) {
    if (digitalRead(emergency_stop_pin) == emergency_isEmergency) {
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< FILTER "));
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, filterN);
      bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F(" ERROR EMERGENCY STOP\n"));
      break;
    }
    if (filter_pos[filter] < filter_goto[filter]) {
      filter_pos[filter]++;
    }
    else {
      filter_pos[filter]--;
    }

    #ifndef DUMMY_SERVO
    filter_servo[filter].write(filter_pos[filter]);
    #else
    Serial.print(F("@ Filter "));
    Serial.print(filter+1);
    Serial.print(" to: ");
    Serial.print(filter_pos[filter]);
    Serial.print('\n');
    #endif

    delay(filter_stepWait);
  }

  filter_go[filter]   = false;
  filter_goto[filter] = 0;

  bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F("< FILTER "));
  bufferWrite(output_buff, sizeof(output_buff), output_buffCount, filterN);
  bufferWrite(output_buff, sizeof(output_buff), output_buffCount, F(" GO FINISHED POS = "));
  char buff[5];
  snprintf(buff,sizeof(buff), "%03d\n", filter_pos[filter]);
  bufferWrite(output_buff, sizeof(output_buff), output_buffCount, buff);

  output_buff_flush();
}
