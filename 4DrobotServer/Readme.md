# Readme for the Arduino code

The Arduino code, meant to be edited and uploaded using the standard Arduino IDE, is found here.
The authors of the code have always used a Linux machine for this.
An Arduino MEGA is required due to the I/O and memory requirements of the code.

The code is split into several files.
A user should only need to change the file `4DrobotServer_config.h`; all configuration flags are set there.
This includes things such as axis assignment and orientation, pin configuration, IP/DHCP configuration, and many other things.
Note that to operate safely (without loosing calibration over time) the Arduino should know the stop-to-stop length of the stepper rails.

The Arduino program is written as a very simple OS, which runs several programs in a round-robin way:
* Communication over Serial and Ethernet
* Control of servos (filter and grabber)
* Control of steppers (X, Y, and Z)
* Readout of thermometers
* Housekeeping

The programs communicate and keep track of their states using global and persistent variables defined in `4DrobotServer_globalVars.h`.
The stepper control program supports acceleration, allowing for reliable and relatively high-speed operation of the stepper motors.

For information about the text-based command protol used, see [Protocol](Protocol.md).
This command protocol is accessible both over Ethernet and USB-serial, supporting concurrent connections from several users.
In general, the `HELP` command is very helpful.

Libraries in use, installable through Arduino IDE:
* [Ethernet](https://www.arduino.cc/reference/en/libraries/ethernet/) - used to interface the Ethernet shield
* [Dallas Temperature](https://www.arduino.cc/reference/en/libraries/dallastemperature/) - used to read the thermometers
* [OneWire](https://www.arduino.cc/reference/en/libraries/onewire/) - used by the the Dallas Temperature library
* [Servo](https://www.arduino.cc/reference/en/libraries/servo/) - For controlling servomotors (optional)
* [Adafruit TiCoServo](https://www.arduino.cc/reference/en/libraries/adafruit-ticoservo/) - For controlling servomotiors (optional)

Note that *at least one* of the two servo libraries must be available.
The TiCoServo library is strongly reccomended, as it avoids "jittering" of the servomotors due to the OneWire library disabling interrupts, it might also reduce waveform jitter for the stepper motors.
